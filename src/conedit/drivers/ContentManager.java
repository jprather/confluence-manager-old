/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package conedit.drivers;

import com.atlassian.confluence.rpc.InvalidSessionException;
import com.atlassian.confluence.rpc.NotPermittedException;
import com.atlassian.confluence.rpc.VersionMismatchException;
import com.atlassian.confluence.rpc.soap.beans.RemotePage;
import com.atlassian.confluence.rpc.soap.beans.RemotePageSummary;
import com.atlassian.confluence.rpc.soap.beans.RemotePageUpdateOptions;
import com.atlassian.confluence.rpc.soap.beans.RemoteSpaceSummary;
import conedit.ConEditApp;
import conedit.ConEditView;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.jdesktop.swingworker.SwingWorker;


public class ContentManager{
    
    //Operation type 0 = add
    //   "        "  " = remove
    int operationType=0;
    
    int numofSpaces=0;
    
    //Add content before = 0;
    //Add content after = 1;
    int beforeorAfter=0;
    
    //Remove type 0 = only first occurence
    //Remove type 1 = only last occurence
    //Remove type 2 = remove all except first
    //Remove type 3 = remove all except last
    //Remove type 4 = remove all
    int removeType=0;
    
    String content="";
    ArrayList<RemoteSpaceSummary> spaceData=null;
    ArrayList<RemotePageSummary> pageData=null;
    ArrayList<RemotePage> finalData= new ArrayList<RemotePage>();
    
    public ContentManager(ArrayList<RemotePageSummary> pages, ArrayList<RemoteSpaceSummary> spaces, String con){
        if(content!=null || content.equals("")){
            content=con;
        }
        if(!spaces.isEmpty()){
            spaceData=spaces;
        }
        if(!pages.isEmpty()){
            pageData=pages;
        }
    }

    public ContentManager() {
        
    }
    
    public void resetData(){
        spaceData=null;
        pageData=null;
        finalData=null;
        beforeorAfter=0;
        numofSpaces=0;
        removeType=0;
        content="";
    }
    
    public void setFinalData(){
    
            class finalDataSetter extends SwingWorker<String[], Object> {
            @Override
            public String[] doInBackground() throws java.rmi.RemoteException {    
        
                 //Set Total # of pages && Progress Bar Max
                 System.out.println("SetFinalData() - Start of progress calculations");
                 int totalpagecount=0;
                 
                 if(spaceData!=null){
                     totalpagecount+=spaceData.size();
                 }else{System.out.println("spaceData is null.");}
                 
                 
                 System.out.println("SetFinalData() - Total Number Of Pages: "+totalpagecount);
                 
                 if(spaceData!=null){
                 for(int i=0;i<spaceData.size();i++){
                       try {
                          totalpagecount+=ConEditView.getActiveService().getPages(ConEditView.getActiveToken(), spaceData.get(i).getKey()).length;
                       } catch (RemoteException ex) {
                    Logger.getLogger(ContentManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                       System.out.println("hit-"+i);  
                 }}
                 
                 System.out.println("SetFinalData() - __Number Of Pages Total: "+totalpagecount);
                 ConEditView.carProgressBar.setValue(0);
                 ConEditView.carProgressBar.setMaximum((totalpagecount*2));
                 ConEditView.carEditedPages.setText("0");
                 ConEditView.carTotalPages.setText(String.valueOf(totalpagecount));
                 System.out.println("SetFinalData() - end of progress calculations");
                 
                 
                 //Space Adder
                 if(spaceData!=null && !spaceData.isEmpty()){
                    System.out.println("SetFinalData() - Converting Spaces To Pages.");
                    for(int i=0;i<spaceData.size();i++){
                        try {
                            RemotePageSummary[] temp = ConEditView.getActiveService().getPages(ConEditView.getActiveToken(), spaceData.get(i).getKey());
                            System.out.println("SetFinalData() - temp size:"+temp.length);
                            System.out.println("SetFinalData() - space:" + spaceData.get(i).getUrl());
                            for(int z=0;z<temp.length;z++){
                                RemotePage data = ConEditView.getActiveService().getPage(ConEditView.getActiveToken(), temp[z].getId());
                                finalData.add(data);
                                ConEditView.carProgressBar.setValue(ConEditView.carProgressBar.getValue()+1);
                            } 
                        } catch (RemoteException ex) {
                            Logger.getLogger(ContentManager.class.getName()).log(Level.SEVERE, null, ex);
                        } 
                    }         
                }else{System.out.println("SetFinalData() - Improper spaceData");}
                
                //Page Adder
                if(pageData!=null && !pageData.isEmpty()){
                    System.out.println("SetFinalData() - Adding Page data");
                    for(int i=0;i<pageData.size();i++){
                        try {
                            finalData.add(ConEditView.getActiveService().getPage(ConEditView.getActiveToken(),pageData.get(i).getId()));
                            ConEditView.carProgressBar.setValue(ConEditView.carProgressBar.getValue()+1);
                        } catch (RemoteException ex) {
                            Logger.getLogger(ContentManager.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                return new String[0];
            }
                @Override
                protected void done(){
                    System.out.println("SetFinalData() - setFinalData Complete");
                    if(operationType==0){
                        performAdd();
                    }else if(operationType==1){
                        performRemove();
                    }
                }
            }
            (new finalDataSetter()).execute();
    }
    
    
    //GLOBAL ADD FUNCTION
    public void performAdd(){
            class performAddThread extends SwingWorker<String[], Object> {
            @Override
            public String[] doInBackground() throws java.rmi.RemoteException {
                
                //Create & Set Update Options
                RemotePageUpdateOptions puo = new RemotePageUpdateOptions();
                puo.setMinorEdit(true);
                puo.setVersionComment("Confluence Manager - Global Add Function");
                System.out.println("PerformAdd() - SwingWorker Entered");
                System.out.println("PerformAdd() - finalData.size():"+finalData.size());
                String newcon="";
                
                //set spacer
                String spacer="";
                if(beforeorAfter==0){
                    for(int i=0;i<numofSpaces;i++){
                        spacer=spacer+" ";
                    }
                    newcon = content+spacer;
                }
                if(beforeorAfter==1){
                    for(int i=0;i<numofSpaces;i++){
                        spacer=" "+spacer;
                    }
                    newcon = spacer+content;
                } 
                for(int i=0;i<finalData.size();i++){             
                    String finalcontent="";
                    if(beforeorAfter==0){finalcontent=newcon+finalData.get(i).getContent();}
                    if(beforeorAfter==1){finalcontent=finalData.get(i).getContent()+newcon;}
                    finalData.get(i).setContent(finalcontent);
                    ConEditView.getActiveService().updatePage(ConEditView.getActiveToken(), finalData.get(i), puo);
                    ConEditView.carProgressBar.setValue(ConEditView.carProgressBar.getValue()+1);
                    
                    //Update Progress Bar & Edited Pages Box
                    ConEditView.carProgressBar.setValue(ConEditView.carProgressBar.getValue()+1);
                    ConEditView.carEditedPages.setText(String.valueOf(Integer.parseInt(ConEditView.carEditedPages.getText())+1));
                 }
                 String[] test = new String[0];
                 return(test);
            }
            @Override
            protected void done(){
                try{
                    System.out.println("PerformADD() - Completed.");
                    finalData = new ArrayList<RemotePage>();
                }catch (Exception ignore) {
                }
            }
            }
            (new performAddThread()).execute();
    }
    
    //GLOBAL REMOVE FUNCTION
    public void performRemove(){
            class performRemoveThread extends SwingWorker<String[], Object> {
            @Override
            public String[] doInBackground() throws java.rmi.RemoteException {
                
                //Create & Set Update Options
                RemotePageUpdateOptions puo = new RemotePageUpdateOptions();
                puo.setMinorEdit(true);
                puo.setVersionComment("Confluence Manager - Global Remove Function");    
                
                System.out.println("PerformAdd() - SwingWorker Entered");
                System.out.println("PerformAdd() - finalData.size():"+finalData.size());

                //Traverse Page Array
                for(int i=0;i<finalData.size();i++){             
                    String finalcontent="";

                    //Operation Selection
                    //RemoveTypes 0-4
                    
                    //Remove First Only
                    if(removeType==0){
                        for(int x=0;x<finalData.size();++x){
                            RemotePage temp = finalData.get(x);
                            String content = temp.getContent();
                            
                            
                        }
                    //Remove Last Only
                    }else if(removeType==1){
                        
                        
                        
                        
                        
                        
                    //remove all except first 
                    }else if(removeType==2){
 
                        
                        
                        
                        
                    //remove all except last
                    }else if(removeType==3){
                        
                        
                        
                        
                        
                    //remove all
                    }else if(removeType==4){
                        
                        
                        
                        
                    }              
                    /*
                    finalData.get(i).setContent(finalcontent);
                    ConEditView.getActiveService().updatePage(ConEditView.getActiveToken(), finalData.get(i), puo);
                    ConEditView.carProgressBar.setValue(ConEditView.carProgressBar.getValue()+1);
                    
                    //Update Progress Bar & Edited Pages Box
                    ConEditView.carProgressBar.setValue(ConEditView.carProgressBar.getValue()+1);
                    ConEditView.carEditedPages.setText(String.valueOf(Integer.parseInt(ConEditView.carEditedPages.getText())+1));
                     Useful bits^^^
                     */
                 }
                
                 String[] test = new String[0];
                 return(test);
            }
            @Override
            protected void done(){
                try{
                    System.out.println("PerformRemove() - Completed.");
                    finalData = new ArrayList<RemotePage>();
                }catch (Exception ignore) {
                }
            }
            }
            (new performRemoveThread()).execute();
    }
    
    public void setSpacesData(ArrayList<RemoteSpaceSummary> space){
        if(!space.isEmpty() && space!=null){
            spaceData=space;
        }
    }
    
    public void setPagesData(ArrayList<RemotePageSummary> pages){
        if(!pages.isEmpty() && pages!=null){
            pageData=pages;
        }
    }
    
    public void setRemoveType(int i){
        if(i>=0 && i<=4){
            removeType=i;
        }else{
            System.out.println("Unsupported Value - ContentAdder");
        }
    }
    public void setNumberOfSpaces(int i){
        if(i>=0 && i<=256){
            numofSpaces=i;
        }else{
            System.out.println("Unsupported Value - ContentAdder");
        }  
    }
    public void setContent(String x){
        if(x!=null){
            content=x;
        }
    }
    public void setBeforeOrAfter(int i){
        if(i>=0 && i<=1){
            beforeorAfter=i;
        }else{
            System.out.println("Unsupported Value - ContentAdder");
        }
    }
    public void setOperationType(int i){
        if(i==0 || i==1){
            operationType=i;
        }else{
            System.out.println("Invalid Operation Type");
        }
    }
    private void Display(String x){
        int mc = JOptionPane.INFORMATION_MESSAGE;
               JOptionPane.showMessageDialog(ConEditView.getMainPanel(),x, "Confluence Manager", mc);
    }

    
}
