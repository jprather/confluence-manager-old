/*
 * ConEditView.java
 */

package conedit;
/*
Copyright [2012] [Jacob Prather]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
import com.atlassian.confluence.rpc.AuthenticationFailedException;
import com.atlassian.confluence.rpc.InvalidSessionException;
import com.atlassian.confluence.rpc.NotPermittedException;
import com.atlassian.confluence.rpc.soap.beans.RemotePage;
import com.atlassian.confluence.rpc.soap.beans.RemotePageSummary;
import com.atlassian.confluence.rpc.soap.beans.RemotePageUpdateOptions;
import com.atlassian.confluence.rpc.soap.beans.RemoteServerInfo;
import com.atlassian.confluence.rpc.soap.beans.RemoteSpaceSummary;
import com.atlassian.confluence.rpc.soap.beans.RemoteUser;
import com.atlassian.confluence.rpc.soap.beans.RemoteUserInformation;
import com.wiki.rpc.soap_axis.confluenceservice_v1.ConfluenceSoapService;
import com.wiki.rpc.soap_axis.confluenceservice_v1.ConfluenceSoapServiceServiceLocator;
import conedit.drivers.ContentManager;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutionException;
import javax.mail.MessagingException;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.rpc.ServiceException;
import java.awt.Component;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.rpc.ServiceException;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Properties;
import java.util.Vector;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import org.apache.axis.components.threadpool.ThreadPool;
import org.apache.commons.io.IOUtils;

/**
 * The application's main frame.
 */
public class ConEditView extends FrameView {

    public ConEditView(SingleFrameApplication app){
        super(app);
        //"de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel"
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
        initComponents();  

        //SET ICON
        URL url;
        url = this.getClass().getResource("/conedit/resources/conman16.png");
        Image my_image = Toolkit.getDefaultToolkit().getImage(url);
        //System.out.println("HIT");
        this.getFrame().setIconImage(my_image);
        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(true);
       
        //Setting up
        nameBox.setEnabled(false);
        fullnameBox.setEnabled(false);
        emailBox.setEnabled(false);
        urltouserBox.setEnabled(false);
        jButton3.setVisible(false);
        javaVersionLabel.setText("Java Version v"+System.getProperty("java.version").substring(0,3));
        versionStatus.setText("ConManager v"+_Version);
        resultsList.removeAll();
        pmReqs.setText("This operation adds the selected permissions, for a user/group you specified, in the spaces you specified.\n" +
            "-Name Box is filled out and Type Selector is set.\n" +
            "-Atleast 1 permsission box is checked.\n" +
            "-Atleast 1 space is added.\n");
        
        //Java Version Checker
        if(System.getProperty("java.version").substring(0,3).equals("1.7") || System.getProperty("java.version").substring(0,3).equals("1.6")){    
        }else{
            Object[] options = {"Yes",
            "No, thanks",};
            JOptionPane n = new JOptionPane();
            int x = JOptionPane.showOptionDialog(mainPanel,
            "Your Java Is Out Of Date. "
            + "Would You Like To Update?",
            "Java Update",
            JOptionPane.YES_NO_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[1]);
            //System.out.println(x);
            if(x==0){
                try
                {
                Process p=Runtime.getRuntime().exec("cmd /c start http://java.com/en/");
                }
                catch(IOException e1) {System.out.println(e1);}
            }
        }
        
        //Confluence Manager Update Checker
        try {
        URL google = new URL("http://dl.dropbox.com/u/83362877/LatestVersion.txt");
        ReadableByteChannel rbc = Channels.newChannel(google.openStream());
        FileOutputStream fos = new FileOutputStream("LatestVersion.txt");
        fos.getChannel().transferFrom(rbc, 0, 1 << 24);
        
        } catch (IOException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
         try{
              FileInputStream fstream = new FileInputStream("LatestVersion.txt");
              DataInputStream in = new DataInputStream(fstream);
              BufferedReader br = new BufferedReader(new InputStreamReader(in));
              String strLine;
            while ((strLine = br.readLine()) != null)   
                {
                      setUpdateVersion(strLine);
                }
                  in.close();
            }catch (Exception e){//Catch exception if any
                System.err.println("Error: " + e.getMessage());
            }
         if(!_Version.equals(getUpdateVersion())){
               availableStatus.setText("Update Available!");
         }else{
             availableStatus.setText("");
         }
         
        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });

        //Set Permissions
        permissionsSummary[0]="VIEWSPACE";
        permissionsSummary[1]="EDITSPACE";
        permissionsSummary[2]="EXPORTPAGE";
        permissionsSummary[3]="SETPAGEPERMISSIONS";
        permissionsSummary[4]="REMOVEPAGE";
        permissionsSummary[5]="EDITBLOG";
        permissionsSummary[6]="REMOVEBLOG";
        permissionsSummary[7]="COMMENT";
        permissionsSummary[8]="REMOVECOMMENT";
        permissionsSummary[9]="CREATEATTACHMENT";
        permissionsSummary[10]="REMOVEATTACHMENT";
        permissionsSummary[11]="REMOVEMAIL";
        permissionsSummary[12]="EXPORTSPACE";
        permissionsSummary[13]="SETSPACEPERMISSIONS";
        
        //Setting Tabs/buttons
        logoutButton.setEnabled(false); 
        
        TaskFlow.setVisible(true);
        TaskFlow.setEnabled(true);
        flipTaskFlow();

        //Set some initial data
        acSubjectBox.setText("Welcome!");
        acMessageBox.setText(EMAIL_TEMPLATE);
        
        seiFileChooser.setControlButtonsAreShown(false);
        carR4.setSelected(true);
        
        for(int i=0;i<257;i++){
            carNumberOfSpaces.addItem(i);
        }
    }
    @Action 
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = ConEditApp.getApplication().getMainFrame();
            aboutBox = new ConEditAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        ConEditApp.getApplication().show(aboutBox);
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        TaskFlow = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        pmUGSelector = new javax.swing.JComboBox();
        pmEntityBox = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        perm1 = new javax.swing.JCheckBox();
        perm2 = new javax.swing.JCheckBox();
        perm3 = new javax.swing.JCheckBox();
        perm4 = new javax.swing.JCheckBox();
        perm5 = new javax.swing.JCheckBox();
        perm6 = new javax.swing.JCheckBox();
        perm7 = new javax.swing.JCheckBox();
        perm8 = new javax.swing.JCheckBox();
        perm9 = new javax.swing.JCheckBox();
        perm10 = new javax.swing.JCheckBox();
        perm11 = new javax.swing.JCheckBox();
        perm12 = new javax.swing.JCheckBox();
        perm13 = new javax.swing.JCheckBox();
        perm14 = new javax.swing.JCheckBox();
        jLabel62 = new javax.swing.JLabel();
        pmSelectAll = new javax.swing.JCheckBox();
        jPanel25 = new javax.swing.JPanel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        pmPermList = new javax.swing.JList();
        pmEntityResultsBox = new javax.swing.JTextField();
        pmSpacesModifiedBox = new javax.swing.JTextField();
        pmSpacesSpecifiedBox = new javax.swing.JTextField();
        jLabel70 = new javax.swing.JLabel();
        pmProgressBar = new javax.swing.JProgressBar();
        pmOperationBox = new javax.swing.JTextField();
        jPanel26 = new javax.swing.JPanel();
        jLabel69 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        pmOperationSelector = new javax.swing.JComboBox();
        jScrollPane7 = new javax.swing.JScrollPane();
        pmReqs = new javax.swing.JTextArea();
        pmPerformButton = new javax.swing.JButton();
        jLabel75 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jPanel24 = new javax.swing.JPanel();
        jLabel63 = new javax.swing.JLabel();
        pmSpaceSelector = new javax.swing.JComboBox();
        jScrollPane5 = new javax.swing.JScrollPane();
        pmSpaces = new javax.swing.JList();
        pmRemoveAll = new javax.swing.JButton();
        pmRemoveSelected = new javax.swing.JButton();
        pmAddAll = new javax.swing.JButton();
        pmAddSpace = new javax.swing.JButton();
        jPanel30 = new javax.swing.JPanel();
        jPanel31 = new javax.swing.JPanel();
        jLabel81 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        carContentArea = new javax.swing.JTextArea();
        jLabel80 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel35 = new javax.swing.JPanel();
        jLabel79 = new javax.swing.JLabel();
        jLabel87 = new javax.swing.JLabel();
        jLabel88 = new javax.swing.JLabel();
        carBeforeOrAfter = new javax.swing.JComboBox();
        carNumberOfSpaces = new javax.swing.JComboBox();
        jPanel36 = new javax.swing.JPanel();
        jLabel86 = new javax.swing.JLabel();
        jLabel90 = new javax.swing.JLabel();
        carR0 = new javax.swing.JCheckBox();
        carR1 = new javax.swing.JCheckBox();
        carR2 = new javax.swing.JCheckBox();
        carR3 = new javax.swing.JCheckBox();
        carR4 = new javax.swing.JCheckBox();
        jPanel29 = new javax.swing.JPanel();
        ssLabel1 = new javax.swing.JLabel();
        carSpaceSelector = new javax.swing.JComboBox();
        carPageSelector = new javax.swing.JComboBox();
        psLabel1 = new javax.swing.JLabel();
        saLabel1 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        carSearchAreaList = new javax.swing.JList();
        carClearButton = new javax.swing.JButton();
        carAddPageButton = new javax.swing.JButton();
        carAddSpaceButton = new javax.swing.JButton();
        carAddAllButton = new javax.swing.JButton();
        jLabel89 = new javax.swing.JLabel();
        carAddContent = new javax.swing.JButton();
        carRemoveContent = new javax.swing.JButton();
        jLabel91 = new javax.swing.JLabel();
        jPanel37 = new javax.swing.JPanel();
        carProgressBar = new javax.swing.JProgressBar();
        jLabel92 = new javax.swing.JLabel();
        carEditedPages = new javax.swing.JTextField();
        jLabel93 = new javax.swing.JLabel();
        carTotalPages = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        advOptionsStatus = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        searchBox = new javax.swing.JTextField();
        replaceBox = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        reasonBox = new javax.swing.JTextField();
        sandrButton = new javax.swing.JButton();
        searchandrButton = new javax.swing.JButton();
        advOptions = new javax.swing.JCheckBox();
        jPanel21 = new javax.swing.JPanel();
        spaceSelector = new javax.swing.JComboBox();
        ssLabel = new javax.swing.JLabel();
        psLabel = new javax.swing.JLabel();
        pageSelector = new javax.swing.JComboBox();
        addSpaceButton = new javax.swing.JButton();
        addPageButton = new javax.swing.JButton();
        clearButton = new javax.swing.JButton();
        saLabel = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        searchAreaList = new javax.swing.JList();
        jPanel22 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        numberoftimesBox = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        searchProgressBar = new javax.swing.JProgressBar();
        jScrollPane2 = new javax.swing.JScrollPane();
        resultsList = new javax.swing.JList();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        currentStatus = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        acIDBox = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        acPasswordBox = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        acFullnameBox = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        acEmailBox = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        acUserGroupsBox = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel76 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        acFWEmail1Box = new javax.swing.JTextField();
        acFWEmail2Box = new javax.swing.JTextField();
        acFWEmail3Box = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        senderEmailBox = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        senderPasswordBox = new javax.swing.JPasswordField();
        jLabel24 = new javax.swing.JLabel();
        senderSMTPBox = new javax.swing.JTextField();
        senderPortBox = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        createButton = new javax.swing.JButton();
        acNotifyUser = new javax.swing.JCheckBox();
        automakeCheckBox = new javax.swing.JCheckBox();
        notificationsCheck = new javax.swing.JCheckBox();
        jLabel14 = new javax.swing.JLabel();
        jPanel28 = new javax.swing.JPanel();
        jLabel77 = new javax.swing.JLabel();
        acSubjectBox = new javax.swing.JTextField();
        jLabel78 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        acMessageBox = new javax.swing.JTextArea();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        groupComboBox = new javax.swing.JComboBox();
        jLabel74 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        autgIDBox = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        autgGroupBox = new javax.swing.JTextField();
        autgButton = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        rufgIDBox = new javax.swing.JTextField();
        rufgGroupBox = new javax.swing.JTextField();
        rufgButton = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        vgIDBox = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        vgButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        vgList = new javax.swing.JList();
        jPanel13 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        dagBox = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        dagReplaceBox = new javax.swing.JTextField();
        dagButton = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        cangNameBox = new javax.swing.JTextField();
        cangButton = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        urlLabel = new javax.swing.JLabel();
        buildLabel = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        versionLabel = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        searchuserBox = new javax.swing.JTextField();
        searchuserButton = new javax.swing.JButton();
        jLabel33 = new javax.swing.JLabel();
        nameBox = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        fullnameBox = new javax.swing.JTextField();
        emailBox = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        urltouserBox = new javax.swing.JTextField();
        edituserButton = new javax.swing.JButton();
        jPanel17 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        adUserBox = new javax.swing.JTextField();
        adDeleteButton = new javax.swing.JButton();
        jLabel72 = new javax.swing.JLabel();
        jLabel73 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        pwIDBox = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        pcPasswordBox = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel56 = new javax.swing.JLabel();
        pSpaceSelector = new javax.swing.JComboBox();
        jLabel60 = new javax.swing.JLabel();
        pVUPSpaceBox = new javax.swing.JTextField();
        jLabel59 = new javax.swing.JLabel();
        pVUPBox = new javax.swing.JTextField();
        pVUPButton = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        pVUPList = new javax.swing.JList();
        jLabel57 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jPanel32 = new javax.swing.JPanel();
        jPanel33 = new javax.swing.JPanel();
        seiExportButton = new javax.swing.JButton();
        seiSpaceSelector = new javax.swing.JComboBox();
        jLabel83 = new javax.swing.JLabel();
        jLabel82 = new javax.swing.JLabel();
        seiDownload = new javax.swing.JButton();
        seiExportBar = new javax.swing.JProgressBar();
        seiDownloadLink = new javax.swing.JTextField();
        seiRelog = new javax.swing.JButton();
        jPanel34 = new javax.swing.JPanel();
        seiFileChooser = new javax.swing.JFileChooser();
        seiImportButton = new javax.swing.JButton();
        seiImportBar = new javax.swing.JProgressBar();
        jLabel84 = new javax.swing.JLabel();
        jLabel85 = new javax.swing.JLabel();
        LoginSystem = new javax.swing.JPanel();
        jPanel27 = new javax.swing.JPanel();
        jLabel54 = new javax.swing.JLabel();
        domainBox = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        loginBox = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        passwordBox = new javax.swing.JPasswordField();
        cfuButton = new javax.swing.JButton();
        exitButton = new javax.swing.JButton();
        logoutButton = new javax.swing.JButton();
        connectButton = new javax.swing.JButton();
        aboutButton = new javax.swing.JButton();
        availableStatus = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        statusLabel = new javax.swing.JLabel();
        javaVersionLabel = new javax.swing.JLabel();
        versionStatus = new javax.swing.JLabel();
        updateStatus = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(conedit.ConEditApp.class).getContext().getResourceMap(ConEditView.class);
        mainPanel.setBackground(resourceMap.getColor("mainPanel.background")); // NOI18N
        mainPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        mainPanel.setMaximumSize(new java.awt.Dimension(880, 900));
        mainPanel.setMinimumSize(new java.awt.Dimension(880, 900));
        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(880, 900));

        TaskFlow.setBackground(resourceMap.getColor("TaskFlow.background")); // NOI18N
        TaskFlow.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        TaskFlow.setMaximumSize(null);
        TaskFlow.setName("TaskFlow"); // NOI18N

        jPanel1.setBackground(resourceMap.getColor("jPanel1.background")); // NOI18N
        jPanel1.setName("jPanel1"); // NOI18N

        jPanel23.setName("jPanel23"); // NOI18N
        jPanel23.setPreferredSize(new java.awt.Dimension(0, 0));

        jLabel58.setFont(resourceMap.getFont("jLabel58.font")); // NOI18N
        jLabel58.setText(resourceMap.getString("jLabel58.text")); // NOI18N
        jLabel58.setName("jLabel58"); // NOI18N

        pmUGSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Username", "Group Name" }));
        pmUGSelector.setName("pmUGSelector"); // NOI18N

        pmEntityBox.setText(resourceMap.getString("pmEntityBox.text")); // NOI18N
        pmEntityBox.setName("pmEntityBox"); // NOI18N

        jLabel61.setFont(resourceMap.getFont("jLabel61.font")); // NOI18N
        jLabel61.setText(resourceMap.getString("jLabel61.text")); // NOI18N
        jLabel61.setName("jLabel61"); // NOI18N

        perm1.setText(resourceMap.getString("perm1.text")); // NOI18N
        perm1.setName("perm1"); // NOI18N

        perm2.setText(resourceMap.getString("perm2.text")); // NOI18N
        perm2.setName("perm2"); // NOI18N

        perm3.setText(resourceMap.getString("perm3.text")); // NOI18N
        perm3.setName("perm3"); // NOI18N

        perm4.setText(resourceMap.getString("perm4.text")); // NOI18N
        perm4.setName("perm4"); // NOI18N

        perm5.setText(resourceMap.getString("perm5.text")); // NOI18N
        perm5.setName("perm5"); // NOI18N

        perm6.setText(resourceMap.getString("perm6.text")); // NOI18N
        perm6.setName("perm6"); // NOI18N

        perm7.setText(resourceMap.getString("perm7.text")); // NOI18N
        perm7.setName("perm7"); // NOI18N

        perm8.setText(resourceMap.getString("perm8.text")); // NOI18N
        perm8.setName("perm8"); // NOI18N

        perm9.setText(resourceMap.getString("perm9.text")); // NOI18N
        perm9.setName("perm9"); // NOI18N

        perm10.setText(resourceMap.getString("perm10.text")); // NOI18N
        perm10.setName("perm10"); // NOI18N

        perm11.setText(resourceMap.getString("perm11.text")); // NOI18N
        perm11.setName("perm11"); // NOI18N

        perm12.setText(resourceMap.getString("perm12.text")); // NOI18N
        perm12.setName("perm12"); // NOI18N

        perm13.setText(resourceMap.getString("perm13.text")); // NOI18N
        perm13.setName("perm13"); // NOI18N

        perm14.setText(resourceMap.getString("perm14.text")); // NOI18N
        perm14.setName("perm14"); // NOI18N

        jLabel62.setFont(resourceMap.getFont("jLabel62.font")); // NOI18N
        jLabel62.setText(resourceMap.getString("jLabel62.text")); // NOI18N
        jLabel62.setName("jLabel62"); // NOI18N

        pmSelectAll.setFont(resourceMap.getFont("pmSelectAll.font")); // NOI18N
        pmSelectAll.setText(resourceMap.getString("pmSelectAll.text")); // NOI18N
        pmSelectAll.setName("pmSelectAll"); // NOI18N
        pmSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pmSelectAllActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addComponent(jLabel62)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pmUGSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pmEntityBox, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel58))
                .addGap(18, 18, 18)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(perm4)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(perm1)
                            .addComponent(perm2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(perm5)
                            .addComponent(perm3))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(perm7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(perm6)
                            .addComponent(perm9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(perm8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel61))
                .addGap(9, 9, 9)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pmSelectAll)
                    .addComponent(perm11)
                    .addComponent(perm10)
                    .addComponent(perm12)
                    .addComponent(perm13)
                    .addComponent(perm14))
                .addContainerGap(80, Short.MAX_VALUE))
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel61)
                            .addComponent(pmSelectAll))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(perm1)
                            .addComponent(perm10)
                            .addComponent(perm8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(perm2)
                            .addComponent(perm11)
                            .addComponent(perm9))
                        .addGap(3, 3, 3)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(perm5)
                            .addComponent(perm12)
                            .addComponent(perm6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(perm3)
                            .addComponent(perm7)
                            .addComponent(perm13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(perm14)
                            .addComponent(perm4)))
                    .addComponent(jLabel58)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addComponent(pmEntityBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel62)
                            .addComponent(pmUGSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(16, 16, 16))
        );

        jPanel25.setMaximumSize(new java.awt.Dimension(688, 203));
        jPanel25.setMinimumSize(new java.awt.Dimension(688, 203));
        jPanel25.setName("jPanel25"); // NOI18N

        jLabel64.setFont(resourceMap.getFont("jLabel64.font")); // NOI18N
        jLabel64.setText(resourceMap.getString("jLabel64.text")); // NOI18N
        jLabel64.setName("jLabel64"); // NOI18N

        jLabel65.setText(resourceMap.getString("jLabel65.text")); // NOI18N
        jLabel65.setName("jLabel65"); // NOI18N

        jLabel66.setText(resourceMap.getString("jLabel66.text")); // NOI18N
        jLabel66.setName("jLabel66"); // NOI18N

        jLabel67.setText(resourceMap.getString("jLabel67.text")); // NOI18N
        jLabel67.setName("jLabel67"); // NOI18N

        jLabel68.setText(resourceMap.getString("jLabel68.text")); // NOI18N
        jLabel68.setName("jLabel68"); // NOI18N

        jScrollPane6.setName("jScrollPane6"); // NOI18N

        pmPermList.setDoubleBuffered(true);
        pmPermList.setName("pmPermList"); // NOI18N
        jScrollPane6.setViewportView(pmPermList);

        pmEntityResultsBox.setEditable(false);
        pmEntityResultsBox.setText(resourceMap.getString("pmEntityResultsBox.text")); // NOI18N
        pmEntityResultsBox.setName("pmEntityResultsBox"); // NOI18N

        pmSpacesModifiedBox.setEditable(false);
        pmSpacesModifiedBox.setText(resourceMap.getString("pmSpacesModifiedBox.text")); // NOI18N
        pmSpacesModifiedBox.setName("pmSpacesModifiedBox"); // NOI18N

        pmSpacesSpecifiedBox.setEditable(false);
        pmSpacesSpecifiedBox.setText(resourceMap.getString("pmSpacesSpecifiedBox.text")); // NOI18N
        pmSpacesSpecifiedBox.setName("pmSpacesSpecifiedBox"); // NOI18N

        jLabel70.setText(resourceMap.getString("jLabel70.text")); // NOI18N
        jLabel70.setName("jLabel70"); // NOI18N

        pmProgressBar.setName("pmProgressBar"); // NOI18N

        pmOperationBox.setText(resourceMap.getString("pmOperationBox.text")); // NOI18N
        pmOperationBox.setEnabled(false);
        pmOperationBox.setName("pmOperationBox"); // NOI18N

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel64, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel25Layout.createSequentialGroup()
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel25Layout.createSequentialGroup()
                                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel67)
                                    .addComponent(jLabel65)
                                    .addComponent(jLabel66))
                                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel25Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(pmEntityResultsBox, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel25Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(pmSpacesModifiedBox, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                                            .addComponent(pmSpacesSpecifiedBox, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                                            .addComponent(pmOperationBox)))))
                            .addComponent(pmProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel70))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
                            .addComponent(jLabel68))))
                .addContainerGap())
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel64)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel68)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(pmProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel70)
                            .addComponent(pmOperationBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel65)
                            .addComponent(pmSpacesSpecifiedBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(pmSpacesModifiedBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel66))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel67)
                            .addComponent(pmEntityResultsBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel26.setName("jPanel26"); // NOI18N

        jLabel69.setFont(resourceMap.getFont("jLabel69.font")); // NOI18N
        jLabel69.setText(resourceMap.getString("jLabel69.text")); // NOI18N
        jLabel69.setName("jLabel69"); // NOI18N

        jLabel71.setFont(resourceMap.getFont("jLabel71.font")); // NOI18N
        jLabel71.setText(resourceMap.getString("jLabel71.text")); // NOI18N
        jLabel71.setName("jLabel71"); // NOI18N

        pmOperationSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Set Permissions", "Remove Permissions", "Set Permissions To Specified Spaces For Anonymous", "Remove Anonymous's Permissions" }));
        pmOperationSelector.setName("pmOperationSelector"); // NOI18N
        pmOperationSelector.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                pmOperationSelectorPropertyChange(evt);
            }
        });

        jScrollPane7.setName("jScrollPane7"); // NOI18N

        pmReqs.setColumns(20);
        pmReqs.setRows(5);
        pmReqs.setAutoscrolls(false);
        pmReqs.setHighlighter(null);
        pmReqs.setName("pmReqs"); // NOI18N
        jScrollPane7.setViewportView(pmReqs);

        pmPerformButton.setForeground(resourceMap.getColor("pmPerformButton.foreground")); // NOI18N
        pmPerformButton.setText(resourceMap.getString("pmPerformButton.text")); // NOI18N
        pmPerformButton.setName("pmPerformButton"); // NOI18N
        pmPerformButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pmPerformButtonActionPerformed(evt);
            }
        });

        jLabel75.setFont(resourceMap.getFont("jLabel75.font")); // NOI18N
        jLabel75.setText(resourceMap.getString("jLabel75.text")); // NOI18N
        jLabel75.setName("jLabel75"); // NOI18N

        jButton4.setText(resourceMap.getString("jButton4.text")); // NOI18N
        jButton4.setName("jButton4"); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel26Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel26Layout.createSequentialGroup()
                        .addComponent(jLabel71)
                        .addGap(18, 18, 18)
                        .addComponent(pmOperationSelector, 0, 605, Short.MAX_VALUE))
                    .addGroup(jPanel26Layout.createSequentialGroup()
                        .addComponent(jLabel69)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 114, Short.MAX_VALUE)
                        .addComponent(jLabel75)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(pmPerformButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel71)
                    .addComponent(pmOperationSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pmPerformButton, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton4)
                    .addComponent(jLabel69)
                    .addComponent(jLabel75))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel24.setName("jPanel24"); // NOI18N

        jLabel63.setFont(resourceMap.getFont("jLabel63.font")); // NOI18N
        jLabel63.setText(resourceMap.getString("jLabel63.text")); // NOI18N
        jLabel63.setName("jLabel63"); // NOI18N

        pmSpaceSelector.setName("pmSpaceSelector"); // NOI18N

        jScrollPane5.setName("jScrollPane5"); // NOI18N

        pmSpaces.setName("pmSpaceArea"); // NOI18N
        jScrollPane5.setViewportView(pmSpaces);

        pmRemoveAll.setText(resourceMap.getString("pmRemoveAll.text")); // NOI18N
        pmRemoveAll.setName("pmRemoveAll"); // NOI18N
        pmRemoveAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pmRemoveAllActionPerformed(evt);
            }
        });

        pmRemoveSelected.setText(resourceMap.getString("pmRemoveSelected.text")); // NOI18N
        pmRemoveSelected.setName("pmRemoveSelected"); // NOI18N
        pmRemoveSelected.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pmRemoveSelectedActionPerformed(evt);
            }
        });

        pmAddAll.setText(resourceMap.getString("pmAddAll.text")); // NOI18N
        pmAddAll.setName("pmAddAll"); // NOI18N
        pmAddAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pmAddAllActionPerformed(evt);
            }
        });

        pmAddSpace.setText(resourceMap.getString("pmAddSpace.text")); // NOI18N
        pmAddSpace.setName("pmAddSpace"); // NOI18N
        pmAddSpace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pmAddSpaceActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel63)
                    .addGroup(jPanel24Layout.createSequentialGroup()
                        .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pmSpaceSelector, 0, 551, Short.MAX_VALUE)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 551, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(pmRemoveAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pmRemoveSelected, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pmAddAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pmAddSpace, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel63)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pmSpaceSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pmAddSpace))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel24Layout.createSequentialGroup()
                        .addComponent(pmAddAll)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pmRemoveSelected)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pmRemoveAll))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel25, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel26, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel23, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 714, Short.MAX_VALUE)
                    .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(39, Short.MAX_VALUE))
        );

        TaskFlow.addTab(resourceMap.getString("jPanel1.TabConstraints.tabTitle"), jPanel1); // NOI18N

        jPanel30.setBackground(resourceMap.getColor("jPanel30.background")); // NOI18N
        jPanel30.setName("jPanel30"); // NOI18N

        jPanel31.setName("jPanel31"); // NOI18N

        jLabel81.setFont(resourceMap.getFont("jLabel81.font")); // NOI18N
        jLabel81.setText(resourceMap.getString("jLabel81.text")); // NOI18N
        jLabel81.setName("jLabel81"); // NOI18N

        jScrollPane9.setName("jScrollPane9"); // NOI18N

        carContentArea.setColumns(20);
        carContentArea.setRows(5);
        carContentArea.setName("carContentArea"); // NOI18N
        jScrollPane9.setViewportView(carContentArea);

        javax.swing.GroupLayout jPanel31Layout = new javax.swing.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel31Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                    .addComponent(jLabel81))
                .addContainerGap())
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel31Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel81)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel80.setFont(resourceMap.getFont("jLabel80.font")); // NOI18N
        jLabel80.setText(resourceMap.getString("jLabel80.text")); // NOI18N
        jLabel80.setName("jLabel80"); // NOI18N

        jTabbedPane1.setName("jTabbedPane1"); // NOI18N

        jPanel35.setName("jPanel35"); // NOI18N

        jLabel79.setFont(resourceMap.getFont("jLabel79.font")); // NOI18N
        jLabel79.setText(resourceMap.getString("jLabel79.text")); // NOI18N
        jLabel79.setName("jLabel79"); // NOI18N

        jLabel87.setText(resourceMap.getString("jLabel87.text")); // NOI18N
        jLabel87.setName("jLabel87"); // NOI18N

        jLabel88.setText(resourceMap.getString("jLabel88.text")); // NOI18N
        jLabel88.setName("jLabel88"); // NOI18N

        carBeforeOrAfter.setMaximumRowCount(2);
        carBeforeOrAfter.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Before/Ontop Current Content", "After/Below Current Content" }));
        carBeforeOrAfter.setName("carBeforeOrAfter"); // NOI18N

        carNumberOfSpaces.setName("carNumberOfSpaces"); // NOI18N

        javax.swing.GroupLayout jPanel35Layout = new javax.swing.GroupLayout(jPanel35);
        jPanel35.setLayout(jPanel35Layout);
        jPanel35Layout.setHorizontalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(carBeforeOrAfter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel88)
                    .addComponent(jLabel87)
                    .addComponent(carNumberOfSpaces, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel79))
                .addContainerGap(408, Short.MAX_VALUE))
        );
        jPanel35Layout.setVerticalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel79)
                .addGap(52, 52, 52)
                .addComponent(jLabel88)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(carBeforeOrAfter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel87)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(carNumberOfSpaces, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(88, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(resourceMap.getString("jPanel35.TabConstraints.tabTitle"), jPanel35); // NOI18N

        jPanel36.setName("jPanel36"); // NOI18N

        jLabel86.setFont(resourceMap.getFont("jLabel86.font")); // NOI18N
        jLabel86.setText(resourceMap.getString("jLabel86.text")); // NOI18N
        jLabel86.setName("jLabel86"); // NOI18N

        jLabel90.setText(resourceMap.getString("jLabel90.text")); // NOI18N
        jLabel90.setName("jLabel90"); // NOI18N

        carR0.setText(resourceMap.getString("carR0.text")); // NOI18N
        carR0.setName("carR0"); // NOI18N

        carR1.setText(resourceMap.getString("carR1.text")); // NOI18N
        carR1.setName("carR1"); // NOI18N

        carR2.setText(resourceMap.getString("carR2.text")); // NOI18N
        carR2.setName("carR2"); // NOI18N

        carR3.setText(resourceMap.getString("carR3.text")); // NOI18N
        carR3.setName("carR3"); // NOI18N

        carR4.setText(resourceMap.getString("carR4.text")); // NOI18N
        carR4.setName("carR4"); // NOI18N

        javax.swing.GroupLayout jPanel36Layout = new javax.swing.GroupLayout(jPanel36);
        jPanel36.setLayout(jPanel36Layout);
        jPanel36Layout.setHorizontalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(carR4)
                    .addComponent(carR3)
                    .addComponent(carR2)
                    .addComponent(carR1)
                    .addComponent(jLabel86)
                    .addComponent(carR0)
                    .addComponent(jLabel90))
                .addContainerGap(381, Short.MAX_VALUE))
        );
        jPanel36Layout.setVerticalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel86)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel90)
                .addGap(7, 7, 7)
                .addComponent(carR0)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(carR1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(carR2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(carR3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(carR4)
                .addContainerGap(96, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(resourceMap.getString("jPanel36.TabConstraints.tabTitle"), jPanel36); // NOI18N

        jPanel29.setName("jPanel29"); // NOI18N

        ssLabel1.setFont(resourceMap.getFont("ssLabel1.font")); // NOI18N
        ssLabel1.setText(resourceMap.getString("ssLabel1.text")); // NOI18N
        ssLabel1.setName("ssLabel1"); // NOI18N

        carSpaceSelector.setMaximumSize(new java.awt.Dimension(57, 21));
        carSpaceSelector.setName("carSpaceSelector"); // NOI18N
        carSpaceSelector.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                carSpaceSelectorItemStateChanged(evt);
            }
        });

        carPageSelector.setMaximumSize(new java.awt.Dimension(56, 20));
        carPageSelector.setName("carPageSelector"); // NOI18N

        psLabel1.setFont(resourceMap.getFont("psLabel1.font")); // NOI18N
        psLabel1.setText(resourceMap.getString("psLabel1.text")); // NOI18N
        psLabel1.setName("psLabel1"); // NOI18N

        saLabel1.setFont(resourceMap.getFont("saLabel1.font")); // NOI18N
        saLabel1.setText(resourceMap.getString("saLabel1.text")); // NOI18N
        saLabel1.setName("saLabel1"); // NOI18N

        jScrollPane10.setName("jScrollPane10"); // NOI18N

        carSearchAreaList.setName("carSearchAreaList"); // NOI18N
        jScrollPane10.setViewportView(carSearchAreaList);

        carClearButton.setText(resourceMap.getString("carClearButton.text")); // NOI18N
        carClearButton.setName("carClearButton"); // NOI18N
        carClearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carClearButtonActionPerformed(evt);
            }
        });

        carAddPageButton.setText(resourceMap.getString("carAddPageButton.text")); // NOI18N
        carAddPageButton.setName("carAddPageButton"); // NOI18N
        carAddPageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carAddPageButtonActionPerformed(evt);
            }
        });

        carAddSpaceButton.setText(resourceMap.getString("carAddSpaceButton.text")); // NOI18N
        carAddSpaceButton.setName("carAddSpaceButton"); // NOI18N
        carAddSpaceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carAddSpaceButtonActionPerformed(evt);
            }
        });

        carAddAllButton.setText(resourceMap.getString("carAddAllButton.text")); // NOI18N
        carAddAllButton.setName("carAddAllButton"); // NOI18N
        carAddAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carAddAllButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel29Layout = new javax.swing.GroupLayout(jPanel29);
        jPanel29.setLayout(jPanel29Layout);
        jPanel29Layout.setHorizontalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel29Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
                    .addComponent(ssLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel29Layout.createSequentialGroup()
                        .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(carSpaceSelector, 0, 566, Short.MAX_VALUE)
                            .addComponent(saLabel1)
                            .addComponent(psLabel1)
                            .addComponent(carPageSelector, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(carAddAllButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                            .addComponent(carAddPageButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                            .addComponent(carClearButton, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                            .addComponent(carAddSpaceButton, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel29Layout.setVerticalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel29Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ssLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(carSpaceSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(carAddSpaceButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(carAddAllButton)
                    .addComponent(psLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(carAddPageButton)
                    .addComponent(carPageSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(saLabel1)
                    .addComponent(carClearButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab(resourceMap.getString("jPanel29.TabConstraints.tabTitle"), jPanel29); // NOI18N

        jLabel89.setFont(resourceMap.getFont("jLabel89.font")); // NOI18N
        jLabel89.setText(resourceMap.getString("jLabel89.text")); // NOI18N
        jLabel89.setName("jLabel89"); // NOI18N

        carAddContent.setText(resourceMap.getString("carAddContent.text")); // NOI18N
        carAddContent.setName("carAddContent"); // NOI18N
        carAddContent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carAddContentActionPerformed(evt);
            }
        });

        carRemoveContent.setText(resourceMap.getString("carRemoveContent.text")); // NOI18N
        carRemoveContent.setEnabled(false);
        carRemoveContent.setName("carRemoveContent"); // NOI18N
        carRemoveContent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carRemoveContentActionPerformed(evt);
            }
        });

        jLabel91.setFont(resourceMap.getFont("jLabel91.font")); // NOI18N
        jLabel91.setText(resourceMap.getString("jLabel91.text")); // NOI18N
        jLabel91.setName("jLabel91"); // NOI18N

        jPanel37.setName("jPanel37"); // NOI18N

        carProgressBar.setName("carProgressBar"); // NOI18N

        jLabel92.setText(resourceMap.getString("jLabel92.text")); // NOI18N
        jLabel92.setName("jLabel92"); // NOI18N

        carEditedPages.setEditable(false);
        carEditedPages.setText(resourceMap.getString("carEditedPages.text")); // NOI18N
        carEditedPages.setName("carEditedPages"); // NOI18N

        jLabel93.setText(resourceMap.getString("jLabel93.text")); // NOI18N
        jLabel93.setName("jLabel93"); // NOI18N

        carTotalPages.setEditable(false);
        carTotalPages.setName("carTotalPages"); // NOI18N

        javax.swing.GroupLayout jPanel37Layout = new javax.swing.GroupLayout(jPanel37);
        jPanel37.setLayout(jPanel37Layout);
        jPanel37Layout.setHorizontalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel37Layout.createSequentialGroup()
                        .addComponent(jLabel92)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(carEditedPages, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(carProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                    .addGroup(jPanel37Layout.createSequentialGroup()
                        .addComponent(jLabel93)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(carTotalPages, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel37Layout.setVerticalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel92)
                    .addComponent(carEditedPages, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel93)
                    .addComponent(carTotalPages, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(carProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel30Layout = new javax.swing.GroupLayout(jPanel30);
        jPanel30.setLayout(jPanel30Layout);
        jPanel30Layout.setHorizontalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel30Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel37, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 714, Short.MAX_VALUE)
                    .addComponent(jLabel89)
                    .addGroup(jPanel30Layout.createSequentialGroup()
                        .addComponent(carAddContent)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(carRemoveContent))
                    .addComponent(jLabel91)
                    .addComponent(jLabel80))
                .addContainerGap())
        );
        jPanel30Layout.setVerticalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel30Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel80)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel89)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(carAddContent)
                    .addComponent(carRemoveContent))
                .addGap(18, 18, 18)
                .addComponent(jLabel91)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(129, Short.MAX_VALUE))
        );

        TaskFlow.addTab(resourceMap.getString("jPanel30.TabConstraints.tabTitle"), jPanel30); // NOI18N

        jPanel2.setBackground(resourceMap.getColor("jPanel2.background")); // NOI18N
        jPanel2.setDoubleBuffered(false);
        jPanel2.setMaximumSize(new java.awt.Dimension(1000, 1000));
        jPanel2.setMinimumSize(new java.awt.Dimension(1, 1));
        jPanel2.setName("jPanel2"); // NOI18N

        jLabel7.setText(resourceMap.getString("jLabel7.text")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N

        advOptionsStatus.setFont(resourceMap.getFont("advOptionsStatus.font")); // NOI18N
        advOptionsStatus.setText(resourceMap.getString("advOptionsStatus.text")); // NOI18N
        advOptionsStatus.setName("advOptionsStatus"); // NOI18N

        jPanel20.setName("jPanel20"); // NOI18N

        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N

        searchBox.setText(resourceMap.getString("searchBox.text")); // NOI18N
        searchBox.setName("searchBox"); // NOI18N

        replaceBox.setText(resourceMap.getString("replaceBox.text")); // NOI18N
        replaceBox.setMaximumSize(null);
        replaceBox.setMinimumSize(null);
        replaceBox.setName("replaceBox"); // NOI18N
        replaceBox.setPreferredSize(null);

        jLabel6.setText(resourceMap.getString("jLabel6.text")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N

        jCheckBox1.setText(resourceMap.getString("jCheckBox1.text")); // NOI18N
        jCheckBox1.setName("jCheckBox1"); // NOI18N
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        reasonBox.setText(resourceMap.getString("reasonBox.text")); // NOI18N
        reasonBox.setToolTipText(resourceMap.getString("reasonBox.toolTipText")); // NOI18N
        reasonBox.setName("reasonBox"); // NOI18N

        sandrButton.setForeground(resourceMap.getColor("sandrButton.foreground")); // NOI18N
        sandrButton.setText(resourceMap.getString("sandrButton.text")); // NOI18N
        sandrButton.setToolTipText(resourceMap.getString("sandrButton.toolTipText")); // NOI18N
        sandrButton.setEnabled(false);
        sandrButton.setName("sandrButton"); // NOI18N
        sandrButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sandrButtonActionPerformed(evt);
            }
        });

        searchandrButton.setForeground(resourceMap.getColor("sandrButton.foreground")); // NOI18N
        searchandrButton.setText(resourceMap.getString("searchandrButton.text")); // NOI18N
        searchandrButton.setName("searchandrButton"); // NOI18N
        searchandrButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchandrButtonActionPerformed(evt);
            }
        });

        advOptions.setText(resourceMap.getString("advOptions.text")); // NOI18N
        advOptions.setName("advOptions"); // NOI18N
        advOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                advOptionsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel20Layout.createSequentialGroup()
                        .addComponent(searchandrButton, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sandrButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(advOptions, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel20Layout.createSequentialGroup()
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(replaceBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(searchBox, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(reasonBox, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox1))
                .addContainerGap(17, Short.MAX_VALUE))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(replaceBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reasonBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchandrButton)
                    .addComponent(sandrButton)
                    .addComponent(advOptions)
                    .addComponent(jCheckBox1))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanel21.setName("jPanel21"); // NOI18N

        spaceSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        spaceSelector.setEnabled(false);
        spaceSelector.setMaximumSize(new java.awt.Dimension(57, 21));
        spaceSelector.setName("spaceSelector"); // NOI18N
        spaceSelector.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                spaceSelectorItemStateChanged(evt);
            }
        });

        ssLabel.setText(resourceMap.getString("ssLabel.text")); // NOI18N
        ssLabel.setEnabled(false);
        ssLabel.setName("ssLabel"); // NOI18N

        psLabel.setText(resourceMap.getString("psLabel.text")); // NOI18N
        psLabel.setEnabled(false);
        psLabel.setName("psLabel"); // NOI18N

        pageSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pageSelector.setEnabled(false);
        pageSelector.setMaximumSize(new java.awt.Dimension(56, 20));
        pageSelector.setName("pageSelector"); // NOI18N

        addSpaceButton.setText(resourceMap.getString("addSpaceButton.text")); // NOI18N
        addSpaceButton.setEnabled(false);
        addSpaceButton.setName("addSpaceButton"); // NOI18N
        addSpaceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSpaceButtonActionPerformed(evt);
            }
        });

        addPageButton.setText(resourceMap.getString("addPageButton.text")); // NOI18N
        addPageButton.setEnabled(false);
        addPageButton.setName("addPageButton"); // NOI18N
        addPageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addPageButtonActionPerformed(evt);
            }
        });

        clearButton.setText(resourceMap.getString("clearButton.text")); // NOI18N
        clearButton.setName("clearButton"); // NOI18N
        clearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearButtonActionPerformed(evt);
            }
        });

        saLabel.setText(resourceMap.getString("saLabel.text")); // NOI18N
        saLabel.setEnabled(false);
        saLabel.setName("saLabel"); // NOI18N

        jScrollPane3.setName("jScrollPane3"); // NOI18N

        searchAreaList.setEnabled(false);
        searchAreaList.setName("searchAreaList"); // NOI18N
        jScrollPane3.setViewportView(searchAreaList);

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
                    .addComponent(ssLabel, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(psLabel, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel21Layout.createSequentialGroup()
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(pageSelector, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(spaceSelector, 0, 566, Short.MAX_VALUE))
                            .addComponent(saLabel))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(clearButton, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                            .addComponent(addSpaceButton, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                            .addComponent(addPageButton, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ssLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spaceSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addSpaceButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(psLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pageSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addPageButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(clearButton)
                    .addComponent(saLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel22.setMaximumSize(new java.awt.Dimension(500, 345));
        jPanel22.setMinimumSize(new java.awt.Dimension(500, 345));
        jPanel22.setName("jPanel22"); // NOI18N
        jPanel22.setPreferredSize(new java.awt.Dimension(500, 345));

        jLabel28.setText(resourceMap.getString("jLabel28.text")); // NOI18N
        jLabel28.setName("jLabel28"); // NOI18N

        numberoftimesBox.setBackground(resourceMap.getColor("numberoftimesBox.background")); // NOI18N
        numberoftimesBox.setEditable(false);
        numberoftimesBox.setText(resourceMap.getString("numberoftimesBox.text")); // NOI18N
        numberoftimesBox.setName("numberoftimesBox"); // NOI18N

        jLabel49.setText(resourceMap.getString("jLabel49.text")); // NOI18N
        jLabel49.setName("jLabel49"); // NOI18N

        searchProgressBar.setToolTipText(resourceMap.getString("searchProgressBar.toolTipText")); // NOI18N
        searchProgressBar.setName("searchProgressBar"); // NOI18N

        jScrollPane2.setMaximumSize(new java.awt.Dimension(500, 345));
        jScrollPane2.setMinimumSize(null);
        jScrollPane2.setName("jScrollPane2"); // NOI18N
        jScrollPane2.setPreferredSize(new java.awt.Dimension(500, 345));

        resultsList.setMaximumSize(new java.awt.Dimension(33, 81));
        resultsList.setName("resultsList"); // NOI18N
        jScrollPane2.setViewportView(resultsList);

        jButton1.setFont(resourceMap.getFont("jButton1.font")); // NOI18N
        jButton1.setText(resourceMap.getString("jButton1.text")); // NOI18N
        jButton1.setToolTipText(resourceMap.getString("jButton1.toolTipText")); // NOI18N
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton3.setText(resourceMap.getString("jButton3.text")); // NOI18N
        jButton3.setName("jButton3"); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        currentStatus.setText(resourceMap.getString("currentStatus.text")); // NOI18N
        currentStatus.setName("currentStatus"); // NOI18N

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel22Layout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(numberoftimesBox, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel49)
                        .addGap(44, 44, 44)
                        .addComponent(currentStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(searchProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, 675, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 675, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel22Layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel28)
                        .addComponent(numberoftimesBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel49))
                    .addComponent(currentStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(searchProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                .addGap(5, 5, 5)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton3))
                .addContainerGap())
        );

        jLabel47.setFont(resourceMap.getFont("jLabel47.font")); // NOI18N
        jLabel47.setText(resourceMap.getString("jLabel47.text")); // NOI18N
        jLabel47.setName("jLabel47"); // NOI18N

        jLabel4.setFont(resourceMap.getFont("jLabel4.font")); // NOI18N
        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N

        jLabel48.setFont(resourceMap.getFont("jLabel48.font")); // NOI18N
        jLabel48.setText(resourceMap.getString("jLabel48.text")); // NOI18N
        jLabel48.setName("jLabel48"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel48)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jPanel20, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel22, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 695, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel7)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel47))
                        .addGap(3947, 3947, 3947)
                        .addComponent(advOptionsStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addGap(133, 133, 133)
                                .addComponent(advOptionsStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(72, 72, 72)
                                .addComponent(jLabel7))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel47)
                                .addGap(11, 11, 11)
                                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel48))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        TaskFlow.addTab(resourceMap.getString("jPanel2.TabConstraints.tabTitle"), jPanel2); // NOI18N

        jPanel4.setBackground(resourceMap.getColor("jPanel4.background")); // NOI18N
        jPanel4.setName("jPanel4"); // NOI18N

        jPanel19.setMaximumSize(new java.awt.Dimension(470, 584));
        jPanel19.setMinimumSize(new java.awt.Dimension(470, 584));
        jPanel19.setName("jPanel19"); // NOI18N

        acIDBox.setText(resourceMap.getString("acIDBox.text")); // NOI18N
        acIDBox.setToolTipText(resourceMap.getString("acIDBox.toolTipText")); // NOI18N
        acIDBox.setName("acIDBox"); // NOI18N

        jLabel15.setText(resourceMap.getString("jLabel15.text")); // NOI18N
        jLabel15.setName("jLabel15"); // NOI18N

        jLabel16.setText(resourceMap.getString("jLabel16.text")); // NOI18N
        jLabel16.setName("jLabel16"); // NOI18N

        acPasswordBox.setText(resourceMap.getString("acPasswordBox.text")); // NOI18N
        acPasswordBox.setName("acPasswordBox"); // NOI18N

        jLabel17.setText(resourceMap.getString("jLabel17.text")); // NOI18N
        jLabel17.setName("jLabel17"); // NOI18N

        acFullnameBox.setText(resourceMap.getString("acFullnameBox.text")); // NOI18N
        acFullnameBox.setName("acFullnameBox"); // NOI18N

        jLabel18.setText(resourceMap.getString("jLabel18.text")); // NOI18N
        jLabel18.setName("jLabel18"); // NOI18N

        acEmailBox.setText(resourceMap.getString("acEmailBox.text")); // NOI18N
        acEmailBox.setName("acEmailBox"); // NOI18N

        jLabel19.setText(resourceMap.getString("jLabel19.text")); // NOI18N
        jLabel19.setName("jLabel19"); // NOI18N

        acUserGroupsBox.setText(resourceMap.getString("acUserGroupsBox.text")); // NOI18N
        acUserGroupsBox.setName("acUserGroupsBox"); // NOI18N

        jLabel26.setText(resourceMap.getString("jLabel26.text")); // NOI18N
        jLabel26.setName("jLabel26"); // NOI18N

        jLabel76.setFont(resourceMap.getFont("jLabel76.font")); // NOI18N
        jLabel76.setText(resourceMap.getString("jLabel76.text")); // NOI18N
        jLabel76.setName("jLabel76"); // NOI18N

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)
                            .addComponent(jLabel18))
                        .addGap(41, 41, 41)
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(acPasswordBox, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                            .addComponent(acIDBox, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                            .addComponent(acFullnameBox, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                            .addComponent(acEmailBox, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(68, 68, 68))
                    .addComponent(jLabel19)
                    .addComponent(jLabel76)
                    .addComponent(jLabel26)
                    .addComponent(acUserGroupsBox, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel76)
                .addGap(12, 12, 12)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(acIDBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(acPasswordBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(acFullnameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(acEmailBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(acUserGroupsBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(184, 184, 184))
        );

        jPanel15.setName("jPanel15"); // NOI18N

        jLabel20.setFont(resourceMap.getFont("jLabel20.font")); // NOI18N
        jLabel20.setText(resourceMap.getString("jLabel20.text")); // NOI18N
        jLabel20.setName("jLabel20"); // NOI18N

        acFWEmail1Box.setText(resourceMap.getString("acFWEmail1Box.text")); // NOI18N
        acFWEmail1Box.setName("acFWEmail1Box"); // NOI18N

        acFWEmail2Box.setText(resourceMap.getString("acFWEmail2Box.text")); // NOI18N
        acFWEmail2Box.setName("acFWEmail2Box"); // NOI18N

        acFWEmail3Box.setText(resourceMap.getString("acFWEmail3Box.text")); // NOI18N
        acFWEmail3Box.setName("acFWEmail3Box"); // NOI18N

        jLabel21.setFont(resourceMap.getFont("jLabel21.font")); // NOI18N
        jLabel21.setText(resourceMap.getString("jLabel21.text")); // NOI18N
        jLabel21.setName("jLabel21"); // NOI18N

        jLabel22.setText(resourceMap.getString("jLabel22.text")); // NOI18N
        jLabel22.setName("jLabel22"); // NOI18N

        senderEmailBox.setText(resourceMap.getString("senderEmailBox.text")); // NOI18N
        senderEmailBox.setName("senderEmailBox"); // NOI18N

        jLabel23.setText(resourceMap.getString("jLabel23.text")); // NOI18N
        jLabel23.setName("jLabel23"); // NOI18N

        senderPasswordBox.setText(resourceMap.getString("senderPasswordBox.text")); // NOI18N
        senderPasswordBox.setName("senderPasswordBox"); // NOI18N

        jLabel24.setText(resourceMap.getString("jLabel24.text")); // NOI18N
        jLabel24.setName("jLabel24"); // NOI18N

        senderSMTPBox.setText(resourceMap.getString("senderSMTPBox.text")); // NOI18N
        senderSMTPBox.setName("senderSMTPBox"); // NOI18N

        senderPortBox.setText(resourceMap.getString("senderPortBox.text")); // NOI18N
        senderPortBox.setName("senderPortBox"); // NOI18N

        jLabel25.setText(resourceMap.getString("jLabel25.text")); // NOI18N
        jLabel25.setName("jLabel25"); // NOI18N

        createButton.setForeground(resourceMap.getColor("createButton.foreground")); // NOI18N
        createButton.setText(resourceMap.getString("createButton.text")); // NOI18N
        createButton.setName("createButton"); // NOI18N
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });

        acNotifyUser.setText(resourceMap.getString("acNotifyUser.text")); // NOI18N
        acNotifyUser.setName("acNotifyUser"); // NOI18N
        acNotifyUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                acNotifyUserActionPerformed(evt);
            }
        });

        automakeCheckBox.setText(resourceMap.getString("automakeCheckBox.text")); // NOI18N
        automakeCheckBox.setName("automakeCheckBox"); // NOI18N
        automakeCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                automakeCheckBoxActionPerformed(evt);
            }
        });

        notificationsCheck.setText(resourceMap.getString("notificationsCheck.text")); // NOI18N
        notificationsCheck.setName("notificationsCheck"); // NOI18N
        notificationsCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                notificationsCheckActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(createButton, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(acNotifyUser, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(notificationsCheck))
                    .addComponent(automakeCheckBox)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21)
                    .addComponent(jLabel24)
                    .addComponent(jLabel25)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel22)
                            .addComponent(jLabel23))
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel15Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(senderPortBox, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(senderSMTPBox, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                    .addComponent(senderPasswordBox, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)))
                            .addGroup(jPanel15Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(senderEmailBox, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))))
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(acFWEmail1Box, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(acFWEmail3Box, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(acFWEmail2Box, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel20)
                .addGap(1, 1, 1)
                .addComponent(acFWEmail1Box, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(acFWEmail2Box, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(acFWEmail3Box, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(senderEmailBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(senderPasswordBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(senderSMTPBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(senderPortBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(acNotifyUser)
                    .addComponent(notificationsCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(automakeCheckBox)
                .addGap(17, 17, 17)
                .addComponent(createButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel14.setFont(resourceMap.getFont("jLabel14.font")); // NOI18N
        jLabel14.setText(resourceMap.getString("jLabel14.text")); // NOI18N
        jLabel14.setName("jLabel14"); // NOI18N

        jPanel28.setName("jPanel28"); // NOI18N

        jLabel77.setText(resourceMap.getString("jLabel77.text")); // NOI18N
        jLabel77.setName("jLabel77"); // NOI18N

        acSubjectBox.setText(resourceMap.getString("acSubjectBox.text")); // NOI18N
        acSubjectBox.setName("acSubjectBox"); // NOI18N

        jLabel78.setText(resourceMap.getString("jLabel78.text")); // NOI18N
        jLabel78.setName("jLabel78"); // NOI18N

        jScrollPane8.setName("jScrollPane8"); // NOI18N

        acMessageBox.setColumns(20);
        acMessageBox.setRows(5);
        acMessageBox.setName("acMessageBox"); // NOI18N
        jScrollPane8.setViewportView(acMessageBox);

        jButton6.setText(resourceMap.getString("jButton6.text")); // NOI18N
        jButton6.setName("jButton6"); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText(resourceMap.getString("jButton7.text")); // NOI18N
        jButton7.setName("jButton7"); // NOI18N

        javax.swing.GroupLayout jPanel28Layout = new javax.swing.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel28Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel28Layout.createSequentialGroup()
                        .addComponent(jLabel77)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(acSubjectBox, javax.swing.GroupLayout.DEFAULT_SIZE, 628, Short.MAX_VALUE))
                    .addGroup(jPanel28Layout.createSequentialGroup()
                        .addComponent(jButton7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 514, Short.MAX_VALUE)
                        .addComponent(jButton6))
                    .addComponent(jLabel78, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel28Layout.setVerticalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(acSubjectBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel77))
                .addGap(18, 18, 18)
                .addComponent(jLabel78)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 314, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton6)
                    .addComponent(jButton7))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel14)
                    .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel19, 0, 408, Short.MAX_VALUE)
                    .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        TaskFlow.addTab(resourceMap.getString("jPanel4.TabConstraints.tabTitle"), jPanel4); // NOI18N

        jPanel6.setBackground(resourceMap.getColor("jPanel6.background")); // NOI18N
        jPanel6.setMaximumSize(new java.awt.Dimension(616, 616));
        jPanel6.setMinimumSize(new java.awt.Dimension(0, 0));
        jPanel6.setName("jPanel6"); // NOI18N

        jPanel8.setName("jPanel8"); // NOI18N

        jLabel46.setFont(resourceMap.getFont("jLabel46.font")); // NOI18N
        jLabel46.setText(resourceMap.getString("jLabel46.text")); // NOI18N
        jLabel46.setName("jLabel46"); // NOI18N

        groupComboBox.setName("groupComboBox"); // NOI18N
        groupComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                groupComboBoxActionPerformed(evt);
            }
        });

        jLabel74.setText(resourceMap.getString("jLabel74.text")); // NOI18N
        jLabel74.setName("jLabel74"); // NOI18N

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel46)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel74))
                    .addComponent(groupComboBox, 0, 453, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46)
                    .addComponent(jLabel74))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(groupComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel9.setMaximumSize(new java.awt.Dimension(236, 140));
        jPanel9.setMinimumSize(new java.awt.Dimension(236, 140));
        jPanel9.setName("jPanel9"); // NOI18N

        jLabel34.setFont(resourceMap.getFont("jLabel34.font")); // NOI18N
        jLabel34.setText(resourceMap.getString("jLabel34.text")); // NOI18N
        jLabel34.setName("jLabel34"); // NOI18N

        jLabel35.setText(resourceMap.getString("jLabel35.text")); // NOI18N
        jLabel35.setName("jLabel35"); // NOI18N

        autgIDBox.setText(resourceMap.getString("autgIDBox.text")); // NOI18N
        autgIDBox.setName("autgIDBox"); // NOI18N

        jLabel36.setText(resourceMap.getString("jLabel36.text")); // NOI18N
        jLabel36.setName("jLabel36"); // NOI18N

        autgGroupBox.setBackground(resourceMap.getColor("autgGroupBox.background")); // NOI18N
        autgGroupBox.setText(resourceMap.getString("autgGroupBox.text")); // NOI18N
        autgGroupBox.setName("autgGroupBox"); // NOI18N

        autgButton.setForeground(resourceMap.getColor("cangButton.foreground")); // NOI18N
        autgButton.setText(resourceMap.getString("autgButton.text")); // NOI18N
        autgButton.setName("autgButton"); // NOI18N
        autgButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autgButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(autgGroupBox, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE))
                    .addComponent(jLabel34)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel35)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(autgIDBox, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE))
                    .addComponent(autgButton, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel34)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(autgIDBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(autgGroupBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(autgButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setMaximumSize(new java.awt.Dimension(236, 133));
        jPanel10.setMinimumSize(new java.awt.Dimension(236, 133));
        jPanel10.setName("jPanel10"); // NOI18N

        jLabel41.setFont(resourceMap.getFont("jLabel41.font")); // NOI18N
        jLabel41.setForeground(resourceMap.getColor("jLabel41.foreground")); // NOI18N
        jLabel41.setText(resourceMap.getString("jLabel41.text")); // NOI18N
        jLabel41.setName("jLabel41"); // NOI18N

        jLabel42.setText(resourceMap.getString("jLabel42.text")); // NOI18N
        jLabel42.setName("jLabel42"); // NOI18N

        jLabel43.setText(resourceMap.getString("jLabel43.text")); // NOI18N
        jLabel43.setName("jLabel43"); // NOI18N

        rufgIDBox.setText(resourceMap.getString("rufgIDBox.text")); // NOI18N
        rufgIDBox.setName("rufgIDBox"); // NOI18N

        rufgGroupBox.setBackground(resourceMap.getColor("rufgGroupBox.background")); // NOI18N
        rufgGroupBox.setText(resourceMap.getString("rufgGroupBox.text")); // NOI18N
        rufgGroupBox.setName("rufgGroupBox"); // NOI18N

        rufgButton.setForeground(resourceMap.getColor("cangButton.foreground")); // NOI18N
        rufgButton.setText(resourceMap.getString("rufgButton.text")); // NOI18N
        rufgButton.setName("rufgButton"); // NOI18N
        rufgButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rufgButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rufgButton, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel41)
                        .addGroup(jPanel10Layout.createSequentialGroup()
                            .addComponent(jLabel42)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(rufgIDBox, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE))
                        .addGroup(jPanel10Layout.createSequentialGroup()
                            .addComponent(jLabel43)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(rufgGroupBox))))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel41)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42)
                    .addComponent(rufgIDBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel43)
                    .addComponent(rufgGroupBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rufgButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel11.setMaximumSize(new java.awt.Dimension(236, 251));
        jPanel11.setMinimumSize(new java.awt.Dimension(236, 251));
        jPanel11.setName("jPanel11"); // NOI18N

        jLabel45.setText(resourceMap.getString("jLabel45.text")); // NOI18N
        jLabel45.setName("jLabel45"); // NOI18N

        vgIDBox.setText(resourceMap.getString("vgIDBox.text")); // NOI18N
        vgIDBox.setName("vgIDBox"); // NOI18N

        jLabel44.setFont(resourceMap.getFont("jLabel44.font")); // NOI18N
        jLabel44.setText(resourceMap.getString("jLabel44.text")); // NOI18N
        jLabel44.setName("jLabel44"); // NOI18N

        vgButton.setForeground(resourceMap.getColor("cangButton.foreground")); // NOI18N
        vgButton.setText(resourceMap.getString("vgButton.text")); // NOI18N
        vgButton.setName("vgButton"); // NOI18N
        vgButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vgButtonActionPerformed(evt);
            }
        });

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        vgList.setName("vgList"); // NOI18N
        jScrollPane1.setViewportView(vgList);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                    .addComponent(vgButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                    .addComponent(jLabel44, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel11Layout.createSequentialGroup()
                        .addComponent(jLabel45)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(vgIDBox, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel44)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel45)
                    .addComponent(vgIDBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(vgButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel13.setMaximumSize(new java.awt.Dimension(227, 168));
        jPanel13.setMinimumSize(new java.awt.Dimension(227, 168));
        jPanel13.setName("jPanel13"); // NOI18N

        jLabel52.setFont(resourceMap.getFont("jLabel52.font")); // NOI18N
        jLabel52.setText(resourceMap.getString("jLabel52.text")); // NOI18N
        jLabel52.setName("jLabel52"); // NOI18N

        jLabel53.setText(resourceMap.getString("jLabel53.text")); // NOI18N
        jLabel53.setName("jLabel53"); // NOI18N

        dagBox.setBackground(resourceMap.getColor("dagBox.background")); // NOI18N
        dagBox.setText(resourceMap.getString("dagBox.text")); // NOI18N
        dagBox.setName("dagBox"); // NOI18N

        jLabel55.setText(resourceMap.getString("jLabel55.text")); // NOI18N
        jLabel55.setName("jLabel55"); // NOI18N

        dagReplaceBox.setText(resourceMap.getString("dagReplaceBox.text")); // NOI18N
        dagReplaceBox.setName("dagReplaceBox"); // NOI18N

        dagButton.setForeground(resourceMap.getColor("cangButton.foreground")); // NOI18N
        dagButton.setText(resourceMap.getString("dagButton.text")); // NOI18N
        dagButton.setName("dagButton"); // NOI18N
        dagButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dagButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(dagButton, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel52)
                        .addContainerGap(38, Short.MAX_VALUE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(dagBox, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                        .addGap(12, 12, 12))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel55)
                        .addContainerGap(119, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                        .addComponent(dagReplaceBox, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel53)
                        .addContainerGap())))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel52)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel53)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dagBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel55)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dagReplaceBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dagButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel12.setMaximumSize(new java.awt.Dimension(227, 122));
        jPanel12.setMinimumSize(new java.awt.Dimension(227, 122));
        jPanel12.setName("jPanel12"); // NOI18N
        jPanel12.setRequestFocusEnabled(false);

        jLabel50.setFont(resourceMap.getFont("jLabel50.font")); // NOI18N
        jLabel50.setText(resourceMap.getString("jLabel50.text")); // NOI18N
        jLabel50.setName("jLabel50"); // NOI18N

        jLabel51.setText(resourceMap.getString("jLabel51.text")); // NOI18N
        jLabel51.setName("jLabel51"); // NOI18N

        cangNameBox.setText(resourceMap.getString("cangNameBox.text")); // NOI18N
        cangNameBox.setName("cangNameBox"); // NOI18N

        cangButton.setForeground(resourceMap.getColor("cangButton.foreground")); // NOI18N
        cangButton.setText(resourceMap.getString("cangButton.text")); // NOI18N
        cangButton.setName("cangButton"); // NOI18N
        cangButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cangButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cangNameBox, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                    .addComponent(jLabel50)
                    .addComponent(jLabel51)
                    .addComponent(cangButton, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel50)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel51)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cangNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cangButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(131, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(130, 130, 130))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(271, Short.MAX_VALUE))
        );

        TaskFlow.addTab(resourceMap.getString("jPanel6.TabConstraints.tabTitle"), jPanel6); // NOI18N

        jPanel5.setBackground(resourceMap.getColor("jPanel5.background")); // NOI18N
        jPanel5.setName("jPanel5"); // NOI18N

        jPanel14.setName("jPanel14"); // NOI18N

        jLabel40.setText(resourceMap.getString("jLabel40.text")); // NOI18N
        jLabel40.setName("jLabel40"); // NOI18N

        urlLabel.setFont(resourceMap.getFont("urlLabel.font")); // NOI18N
        urlLabel.setText(resourceMap.getString("urlLabel.text")); // NOI18N
        urlLabel.setName("urlLabel"); // NOI18N

        buildLabel.setFont(resourceMap.getFont("buildLabel.font")); // NOI18N
        buildLabel.setText(resourceMap.getString("buildLabel.text")); // NOI18N
        buildLabel.setName("buildLabel"); // NOI18N

        jLabel39.setText(resourceMap.getString("jLabel39.text")); // NOI18N
        jLabel39.setName("jLabel39"); // NOI18N

        jLabel37.setText(resourceMap.getString("jLabel37.text")); // NOI18N
        jLabel37.setName("jLabel37"); // NOI18N

        versionLabel.setFont(resourceMap.getFont("versionLabel.font")); // NOI18N
        versionLabel.setText(resourceMap.getString("versionLabel.text")); // NOI18N
        versionLabel.setName("versionLabel"); // NOI18N

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel40)
                        .addGap(18, 18, 18)
                        .addComponent(urlLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel37)
                            .addComponent(jLabel39))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(buildLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(versionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(versionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(buildLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel40)
                    .addComponent(urlLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jLabel38.setFont(resourceMap.getFont("jLabel38.font")); // NOI18N
        jLabel38.setText(resourceMap.getString("jLabel38.text")); // NOI18N
        jLabel38.setName("jLabel38"); // NOI18N

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(92, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel38, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(86, 86, 86))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel38)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(727, Short.MAX_VALUE))
        );

        TaskFlow.addTab(resourceMap.getString("jPanel5.TabConstraints.tabTitle"), jPanel5); // NOI18N

        jPanel3.setBackground(resourceMap.getColor("jPanel3.background")); // NOI18N
        jPanel3.setMaximumSize(null);
        jPanel3.setName("jPanel3"); // NOI18N

        jPanel16.setName("jPanel16"); // NOI18N

        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        searchuserBox.setText(resourceMap.getString("searchuserBox.text")); // NOI18N
        searchuserBox.setName("searchuserBox"); // NOI18N

        searchuserButton.setFont(resourceMap.getFont("searchuserButton.font")); // NOI18N
        searchuserButton.setForeground(resourceMap.getColor("edituserButton.foreground")); // NOI18N
        searchuserButton.setText(resourceMap.getString("searchuserButton.text")); // NOI18N
        searchuserButton.setName("searchuserButton"); // NOI18N
        searchuserButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchuserButtonActionPerformed(evt);
            }
        });

        jLabel33.setFont(resourceMap.getFont("jLabel33.font")); // NOI18N
        jLabel33.setText(resourceMap.getString("jLabel33.text")); // NOI18N
        jLabel33.setName("jLabel33"); // NOI18N

        nameBox.setBackground(resourceMap.getColor("nameBox.background")); // NOI18N
        nameBox.setEditable(false);
        nameBox.setText(resourceMap.getString("nameBox.text")); // NOI18N
        nameBox.setEnabled(false);
        nameBox.setName("nameBox"); // NOI18N

        jLabel8.setText(resourceMap.getString("jLabel8.text")); // NOI18N
        jLabel8.setName("jLabel8"); // NOI18N

        jLabel9.setText(resourceMap.getString("jLabel9.text")); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N

        fullnameBox.setText(resourceMap.getString("fullnameBox.text")); // NOI18N
        fullnameBox.setName("fullnameBox"); // NOI18N

        emailBox.setText(resourceMap.getString("emailBox.text")); // NOI18N
        emailBox.setName("emailBox"); // NOI18N

        jLabel10.setText(resourceMap.getString("jLabel10.text")); // NOI18N
        jLabel10.setName("jLabel10"); // NOI18N

        jLabel11.setText(resourceMap.getString("jLabel11.text")); // NOI18N
        jLabel11.setName("jLabel11"); // NOI18N

        urltouserBox.setBackground(resourceMap.getColor("urltouserBox.background")); // NOI18N
        urltouserBox.setEditable(false);
        urltouserBox.setText(resourceMap.getString("urltouserBox.text")); // NOI18N
        urltouserBox.setEnabled(false);
        urltouserBox.setName("urltouserBox"); // NOI18N

        edituserButton.setForeground(resourceMap.getColor("edituserButton.foreground")); // NOI18N
        edituserButton.setText(resourceMap.getString("edituserButton.text")); // NOI18N
        edituserButton.setName("edituserButton"); // NOI18N
        edituserButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edituserButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel8)
                            .addComponent(jLabel3))
                        .addGap(36, 36, 36)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel16Layout.createSequentialGroup()
                                .addComponent(urltouserBox, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edituserButton))
                            .addComponent(emailBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
                            .addComponent(fullnameBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
                            .addComponent(nameBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel16Layout.createSequentialGroup()
                                .addComponent(searchuserBox, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(searchuserButton, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jLabel33))
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(searchuserBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(searchuserButton)))
                .addGap(6, 6, 6)
                .addComponent(jLabel33)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel9)
                    .addComponent(fullnameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emailBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(urltouserBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(edituserButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel17.setName("jPanel17"); // NOI18N

        jLabel29.setText(resourceMap.getString("jLabel29.text")); // NOI18N
        jLabel29.setName("jLabel29"); // NOI18N

        adUserBox.setText(resourceMap.getString("adUserBox.text")); // NOI18N
        adUserBox.setName("adUserBox"); // NOI18N

        adDeleteButton.setForeground(resourceMap.getColor("edituserButton.foreground")); // NOI18N
        adDeleteButton.setText(resourceMap.getString("adDeleteButton.text")); // NOI18N
        adDeleteButton.setName("adDeleteButton"); // NOI18N
        adDeleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adDeleteButtonActionPerformed(evt);
            }
        });

        jLabel72.setText(resourceMap.getString("jLabel72.text")); // NOI18N
        jLabel72.setName("jLabel72"); // NOI18N

        jLabel73.setText(resourceMap.getString("jLabel73.text")); // NOI18N
        jLabel73.setName("jLabel73"); // NOI18N

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(adUserBox, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE))
                    .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel73, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel72, javax.swing.GroupLayout.Alignment.LEADING))
                    .addComponent(adDeleteButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(adUserBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel72)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel73)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(adDeleteButton)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        jPanel18.setName("jPanel18"); // NOI18N

        jLabel31.setText(resourceMap.getString("jLabel31.text")); // NOI18N
        jLabel31.setName("jLabel31"); // NOI18N

        pwIDBox.setText(resourceMap.getString("pwIDBox.text")); // NOI18N
        pwIDBox.setName("pwIDBox"); // NOI18N

        jLabel32.setText(resourceMap.getString("jLabel32.text")); // NOI18N
        jLabel32.setName("jLabel32"); // NOI18N

        pcPasswordBox.setText(resourceMap.getString("pcPasswordBox.text")); // NOI18N
        pcPasswordBox.setName("pcPasswordBox"); // NOI18N

        jButton2.setForeground(resourceMap.getColor("edituserButton.foreground")); // NOI18N
        jButton2.setText(resourceMap.getString("jButton2.text")); // NOI18N
        jButton2.setName("jButton2"); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel32)
                            .addComponent(jLabel31))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(pwIDBox, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                            .addComponent(pcPasswordBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)))
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addGap(118, 118, 118)
                        .addComponent(jButton2)))
                .addContainerGap(68, Short.MAX_VALUE))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(pwIDBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(pcPasswordBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        jPanel7.setName("jPanel7"); // NOI18N

        jLabel56.setFont(resourceMap.getFont("jLabel56.font")); // NOI18N
        jLabel56.setText(resourceMap.getString("jLabel56.text")); // NOI18N
        jLabel56.setName("jLabel56"); // NOI18N

        pSpaceSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pSpaceSelector.setMinimumSize(new java.awt.Dimension(0, 0));
        pSpaceSelector.setName("pSpaceSelector"); // NOI18N
        pSpaceSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pSpaceSelectorActionPerformed(evt);
            }
        });

        jLabel60.setText(resourceMap.getString("jLabel60.text")); // NOI18N
        jLabel60.setName("jLabel60"); // NOI18N

        pVUPSpaceBox.setText(resourceMap.getString("pVUPSpaceBox.text")); // NOI18N
        pVUPSpaceBox.setName("pVUPSpaceBox"); // NOI18N

        jLabel59.setText(resourceMap.getString("jLabel59.text")); // NOI18N
        jLabel59.setName("jLabel59"); // NOI18N

        pVUPBox.setText(resourceMap.getString("pVUPBox.text")); // NOI18N
        pVUPBox.setName("pVUPBox"); // NOI18N

        pVUPButton.setForeground(resourceMap.getColor("edituserButton.foreground")); // NOI18N
        pVUPButton.setText(resourceMap.getString("pVUPButton.text")); // NOI18N
        pVUPButton.setName("pVUPButton"); // NOI18N
        pVUPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pVUPButtonActionPerformed(evt);
            }
        });

        jScrollPane4.setName("jScrollPane4"); // NOI18N

        pVUPList.setName("pVUPList"); // NOI18N
        jScrollPane4.setViewportView(pVUPList);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(pSpaceSelector, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(139, 139, 139)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel59)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pVUPBox))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(149, 149, 149)
                                .addComponent(pVUPButton))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel60)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pVUPSpaceBox, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel56, javax.swing.GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel56)
                .addGap(11, 11, 11)
                .addComponent(pSpaceSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel60)
                    .addComponent(pVUPSpaceBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel59)
                    .addComponent(pVUPBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(pVUPButton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jLabel57.setFont(resourceMap.getFont("jLabel57.font")); // NOI18N
        jLabel57.setText(resourceMap.getString("jLabel57.text")); // NOI18N
        jLabel57.setName("jLabel57"); // NOI18N

        jLabel12.setFont(resourceMap.getFont("jLabel12.font")); // NOI18N
        jLabel12.setText(resourceMap.getString("jLabel12.text")); // NOI18N
        jLabel12.setName("jLabel12"); // NOI18N

        jLabel27.setFont(resourceMap.getFont("jLabel27.font")); // NOI18N
        jLabel27.setText(resourceMap.getString("jLabel27.text")); // NOI18N
        jLabel27.setName("jLabel27"); // NOI18N

        jLabel30.setFont(resourceMap.getFont("jLabel30.font")); // NOI18N
        jLabel30.setText(resourceMap.getString("jLabel30.text")); // NOI18N
        jLabel30.setName("jLabel30"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel57)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel27))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel30)
                            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addComponent(jLabel57)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        TaskFlow.addTab(resourceMap.getString("jPanel3.TabConstraints.tabTitle"), jPanel3); // NOI18N

        jPanel32.setBackground(resourceMap.getColor("jPanel32.background")); // NOI18N
        jPanel32.setName("jPanel32"); // NOI18N

        jPanel33.setName("jPanel33"); // NOI18N

        seiExportButton.setText(resourceMap.getString("seiExportButton.text")); // NOI18N
        seiExportButton.setName("seiExportButton"); // NOI18N
        seiExportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seiExportButtonActionPerformed(evt);
            }
        });

        seiSpaceSelector.setName("seiSpaceSelector"); // NOI18N

        jLabel83.setText(resourceMap.getString("jLabel83.text")); // NOI18N
        jLabel83.setName("jLabel83"); // NOI18N

        jLabel82.setText(resourceMap.getString("jLabel82.text")); // NOI18N
        jLabel82.setName("jLabel82"); // NOI18N

        seiDownload.setText(resourceMap.getString("seiDownload.text")); // NOI18N
        seiDownload.setName("seiDownload"); // NOI18N
        seiDownload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seiDownloadActionPerformed(evt);
            }
        });

        seiExportBar.setName("seiExportBar"); // NOI18N

        seiDownloadLink.setEditable(false);
        seiDownloadLink.setText(resourceMap.getString("seiDownloadLink.text")); // NOI18N
        seiDownloadLink.setName("seiDownloadLink"); // NOI18N

        seiRelog.setText(resourceMap.getString("seiRelog.text")); // NOI18N
        seiRelog.setName("seiRelog"); // NOI18N
        seiRelog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seiRelogActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel82)
                    .addComponent(seiSpaceSelector, 0, 694, Short.MAX_VALUE)
                    .addComponent(seiDownloadLink, javax.swing.GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                    .addComponent(jLabel83)
                    .addComponent(seiDownload, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(seiExportBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                    .addGroup(jPanel33Layout.createSequentialGroup()
                        .addComponent(seiExportButton, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(seiRelog, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jLabel82)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(seiSpaceSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(seiExportBar, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(seiExportButton)
                    .addComponent(seiRelog))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addComponent(jLabel83)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(seiDownloadLink, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(seiDownload)
                .addGap(30, 30, 30))
        );

        jPanel34.setName("jPanel34"); // NOI18N

        seiFileChooser.setBackground(resourceMap.getColor("seiFileChooser.background")); // NOI18N
        seiFileChooser.setCurrentDirectory(new java.io.File("C:\\"));
            seiFileChooser.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            seiFileChooser.setName("seiFileChooser"); // NOI18N
            seiFileChooser.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    seiFileChooserActionPerformed(evt);
                }
            });

            seiImportButton.setText(resourceMap.getString("seiImportButton.text")); // NOI18N
            seiImportButton.setName("seiImportButton"); // NOI18N
            seiImportButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    seiImportButtonActionPerformed(evt);
                }
            });

            seiImportBar.setName("seiImportBar"); // NOI18N

            javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
            jPanel34.setLayout(jPanel34Layout);
            jPanel34Layout.setHorizontalGroup(
                jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel34Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel34Layout.createSequentialGroup()
                            .addComponent(seiFileChooser, javax.swing.GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                            .addContainerGap())
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel34Layout.createSequentialGroup()
                            .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(seiImportBar, javax.swing.GroupLayout.DEFAULT_SIZE, 684, Short.MAX_VALUE)
                                .addComponent(seiImportButton, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(20, 20, 20))))
            );
            jPanel34Layout.setVerticalGroup(
                jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel34Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(seiFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(seiImportButton)
                    .addGap(18, 18, 18)
                    .addComponent(seiImportBar, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(19, 19, 19))
            );

            jLabel84.setFont(resourceMap.getFont("jLabel84.font")); // NOI18N
            jLabel84.setText(resourceMap.getString("jLabel84.text")); // NOI18N
            jLabel84.setName("jLabel84"); // NOI18N

            jLabel85.setFont(resourceMap.getFont("jLabel85.font")); // NOI18N
            jLabel85.setText(resourceMap.getString("jLabel85.text")); // NOI18N
            jLabel85.setName("jLabel85"); // NOI18N

            javax.swing.GroupLayout jPanel32Layout = new javax.swing.GroupLayout(jPanel32);
            jPanel32.setLayout(jPanel32Layout);
            jPanel32Layout.setHorizontalGroup(
                jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel32Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel33, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel84)
                        .addComponent(jPanel34, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel85))
                    .addContainerGap())
            );
            jPanel32Layout.setVerticalGroup(
                jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel32Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel84)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
                    .addComponent(jLabel85)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
            );

            TaskFlow.addTab(resourceMap.getString("jPanel32.TabConstraints.tabTitle"), jPanel32); // NOI18N

            LoginSystem.setBackground(resourceMap.getColor("LoginSystem.background")); // NOI18N
            LoginSystem.setEnabled(false);
            LoginSystem.setMaximumSize(new java.awt.Dimension(1920, 1080));
            LoginSystem.setMinimumSize(new java.awt.Dimension(0, 0));
            LoginSystem.setName("LoginSystem"); // NOI18N

            jPanel27.setName("jPanel27"); // NOI18N

            jLabel54.setText(resourceMap.getString("jLabel54.text")); // NOI18N
            jLabel54.setName("jLabel54"); // NOI18N

            domainBox.setText(resourceMap.getString("domainBox.text")); // NOI18N
            domainBox.setName("domainBox"); // NOI18N

            jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
            jLabel1.setName("jLabel1"); // NOI18N

            loginBox.setText(resourceMap.getString("loginBox.text")); // NOI18N
            loginBox.setName("loginBox"); // NOI18N

            jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
            jLabel2.setName("jLabel2"); // NOI18N

            passwordBox.setText(resourceMap.getString("passwordBox.text")); // NOI18N
            passwordBox.setToolTipText(resourceMap.getString("passwordBox.toolTipText")); // NOI18N
            passwordBox.setName("passwordBox"); // NOI18N

            cfuButton.setForeground(resourceMap.getColor("logoutButton.foreground")); // NOI18N
            cfuButton.setText(resourceMap.getString("cfuButton.text")); // NOI18N
            cfuButton.setName("cfuButton"); // NOI18N
            cfuButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    cfuButtonActionPerformed(evt);
                }
            });

            javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(conedit.ConEditApp.class).getContext().getActionMap(ConEditView.class, this);
            exitButton.setAction(actionMap.get("quit")); // NOI18N
            exitButton.setForeground(resourceMap.getColor("logoutButton.foreground")); // NOI18N
            exitButton.setText(resourceMap.getString("exitButton.text")); // NOI18N
            exitButton.setName("exitButton"); // NOI18N
            exitButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    exitButtonActionPerformed(evt);
                }
            });

            logoutButton.setForeground(resourceMap.getColor("logoutButton.foreground")); // NOI18N
            logoutButton.setText(resourceMap.getString("logoutButton.text")); // NOI18N
            logoutButton.setName("logoutButton"); // NOI18N
            logoutButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    logoutButtonActionPerformed(evt);
                }
            });

            connectButton.setForeground(resourceMap.getColor("logoutButton.foreground")); // NOI18N
            connectButton.setText(resourceMap.getString("connectButton.text")); // NOI18N
            connectButton.setName("connectButton"); // NOI18N
            connectButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    connectButtonActionPerformed(evt);
                }
            });

            aboutButton.setAction(actionMap.get("showAboutBox")); // NOI18N
            aboutButton.setForeground(resourceMap.getColor("logoutButton.foreground")); // NOI18N
            aboutButton.setText(resourceMap.getString("aboutButton.text")); // NOI18N
            aboutButton.setName("aboutButton"); // NOI18N

            availableStatus.setText(resourceMap.getString("availableStatus.text")); // NOI18N
            availableStatus.setName("availableStatus"); // NOI18N

            jLabel13.setFont(resourceMap.getFont("jLabel13.font")); // NOI18N
            jLabel13.setText(resourceMap.getString("jLabel13.text")); // NOI18N
            jLabel13.setVerticalAlignment(javax.swing.SwingConstants.TOP);
            jLabel13.setName("jLabel13"); // NOI18N

            statusLabel.setFont(resourceMap.getFont("statusLabel.font")); // NOI18N
            statusLabel.setForeground(resourceMap.getColor("statusLabel.foreground")); // NOI18N
            statusLabel.setText(resourceMap.getString("statusLabel.text")); // NOI18N
            statusLabel.setName("statusLabel"); // NOI18N

            javaVersionLabel.setText(resourceMap.getString("javaVersionLabel.text")); // NOI18N
            javaVersionLabel.setName("javaVersionLabel"); // NOI18N

            versionStatus.setText(resourceMap.getString("versionStatus.text")); // NOI18N
            versionStatus.setName("versionStatus"); // NOI18N

            updateStatus.setFont(resourceMap.getFont("updateStatus.font")); // NOI18N
            updateStatus.setText(resourceMap.getString("updateStatus.text")); // NOI18N
            updateStatus.setName("updateStatus"); // NOI18N

            javax.swing.GroupLayout jPanel27Layout = new javax.swing.GroupLayout(jPanel27);
            jPanel27.setLayout(jPanel27Layout);
            jPanel27Layout.setHorizontalGroup(
                jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel27Layout.createSequentialGroup()
                    .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel27Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(javaVersionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(versionStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel27Layout.createSequentialGroup()
                                    .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel27Layout.createSequentialGroup()
                                            .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanel27Layout.createSequentialGroup()
                                                    .addComponent(jLabel2)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(passwordBox, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE))
                                                .addGroup(jPanel27Layout.createSequentialGroup()
                                                    .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel1)
                                                        .addComponent(jLabel54))
                                                    .addGap(23, 23, 23)
                                                    .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(domainBox, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                                                        .addComponent(loginBox, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE))))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(logoutButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(connectButton, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                        .addGroup(jPanel27Layout.createSequentialGroup()
                                            .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                            .addGap(226, 226, 226)))
                                    .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(updateStatus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(aboutButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(cfuButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(exitButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)))))
                        .addGroup(jPanel27Layout.createSequentialGroup()
                            .addGap(221, 221, 221)
                            .addComponent(jLabel13)))
                    .addContainerGap())
                .addGroup(jPanel27Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(availableStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                    .addGap(447, 447, 447))
            );
            jPanel27Layout.setVerticalGroup(
                jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel27Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel27Layout.createSequentialGroup()
                            .addComponent(jLabel13)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(availableStatus)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(versionStatus)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(javaVersionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(statusLabel))
                        .addComponent(updateStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel27Layout.createSequentialGroup()
                            .addComponent(aboutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cfuButton)
                                .addComponent(connectButton))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(exitButton)
                                .addComponent(logoutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel27Layout.createSequentialGroup()
                            .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel54)
                                .addComponent(domainBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(loginBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(passwordBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addContainerGap())
            );

            javax.swing.GroupLayout LoginSystemLayout = new javax.swing.GroupLayout(LoginSystem);
            LoginSystem.setLayout(LoginSystemLayout);
            LoginSystemLayout.setHorizontalGroup(
                LoginSystemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(LoginSystemLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(174, Short.MAX_VALUE))
            );
            LoginSystemLayout.setVerticalGroup(
                LoginSystemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(LoginSystemLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(666, Short.MAX_VALUE))
            );

            TaskFlow.addTab(resourceMap.getString("LoginSystem.TabConstraints.tabTitle"), LoginSystem); // NOI18N

            javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
            mainPanel.setLayout(mainPanelLayout);
            mainPanelLayout.setHorizontalGroup(
                mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(mainPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(TaskFlow, javax.swing.GroupLayout.PREFERRED_SIZE, 876, Short.MAX_VALUE)
                    .addContainerGap())
            );
            mainPanelLayout.setVerticalGroup(
                mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(mainPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(TaskFlow, javax.swing.GroupLayout.PREFERRED_SIZE, 902, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(40, Short.MAX_VALUE))
            );

            menuBar.setName("menuBar"); // NOI18N

            fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
            fileMenu.setName("fileMenu"); // NOI18N

            exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
            exitMenuItem.setName("exitMenuItem"); // NOI18N
            fileMenu.add(exitMenuItem);

            menuBar.add(fileMenu);

            helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
            helpMenu.setName("helpMenu"); // NOI18N

            aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
            aboutMenuItem.setName("aboutMenuItem"); // NOI18N
            helpMenu.add(aboutMenuItem);

            menuBar.add(helpMenu);

            statusPanel.setBackground(resourceMap.getColor("statusPanel.background")); // NOI18N
            statusPanel.setName("statusPanel"); // NOI18N

            statusMessageLabel.setName("statusMessageLabel"); // NOI18N

            statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

            progressBar.setName("progressBar"); // NOI18N

            javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
            statusPanel.setLayout(statusPanelLayout);
            statusPanelLayout.setHorizontalGroup(
                statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(statusPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(statusMessageLabel)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 726, Short.MAX_VALUE)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(statusAnimationLabel)
                    .addContainerGap())
            );
            statusPanelLayout.setVerticalGroup(
                statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, statusPanelLayout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(statusMessageLabel)
                        .addComponent(statusAnimationLabel)
                        .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(3, 3, 3))
            );

            setComponent(mainPanel);
            setMenuBar(menuBar);
            setStatusBar(statusPanel);
        }// </editor-fold>//GEN-END:initComponents

    private void connectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectButtonActionPerformed
    statusLabel.setText("Attempting To Connect...");
    
        if(!loginBox.getText().equals("")){
            
            myDomain=domainBox.getText();
            boolean dTest = domainCheck(myDomain);
            
            if(dTest){
                if(loginToSystem()){
                    try {  
                        RemoteUserInformation userinfo = getActiveService().getUserInformation(getActiveToken(), loginBox.getText());
                        statusLabel.setText(userinfo.getUsername() + " Is Logged In.");
                        Display("Successful Login. Give Me A Second To Load Data.");
                        
                        flipLoginSystem(true);

                        //get server info
                        RemoteServerInfo sinfo = getActiveService().getServerInfo(getActiveToken());
                        versionLabel.setText(sinfo.getMajorVersion()+"."+sinfo.getMinorVersion()+"."+sinfo.getPatchLevel());
                        buildLabel.setText(sinfo.getBuildId());
                        urlLabel.setText(sinfo.getBaseUrl());

                        //Set up
                        spaceSummary = getActiveService().getSpaces(getActiveToken());
                        resultsList.setListData(setResultsListDisplaySF());
                        searchAreaList.setListData(setSearchAreaDisplaySF());

                        //fill up space selectors
                        pSpaceSelector.removeAllItems();
                        for(int i=0;i<spaceSummary.length;i++){
                            pSpaceSelector.addItem(spaceSummary[i].getName());
                            pmSpaceSelector.addItem(spaceSummary[i].getName());
                            seiSpaceSelector.addItem(spaceSummary[i].getName());
                            carSpaceSelector.addItem(spaceSummary[i].getName());
                        }
 
                        
                        //retrieve group information and set selectors
                        String[] x;
                        x = getActiveService().getGroups(getActiveToken());
                        Arrays.sort(x);
                        for(int i=0;i<x.length;i++){
                            groupComboBox.addItem(x[i]);
                        }

                        new Timer(delay10, RelogTask).start();

                    } catch (RemoteException ex) {
                        Display("Remote Exception Error2");
                        Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else{
                Display("Unknown Host/Domain.");
                statusLabel.setText("Connect Failed. Waiting For Login..");
             }
        }else{
            Display("A Required Field Was Left Blank.");
        }
}//GEN-LAST:event_connectButtonActionPerformed

    private void logoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutButtonActionPerformed
        try {
            String id = loginBox.getText();
            
            //Logout function
            ConfluenceSoapService service = getActiveService();
            service.logout(getActiveToken());
            
            //Set Appropriate viewing options
            flipLoginSystem(false);
            
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_logoutButtonActionPerformed

    private void exitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_exitButtonActionPerformed

    private void cfuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cfuButtonActionPerformed
        try {
            // TODO add your handling code here:
            URL google = new URL("http://dl.dropbox.com/u/83362877/LatestVersion.txt");
            ReadableByteChannel rbc = Channels.newChannel(google.openStream());
            FileOutputStream fos = new FileOutputStream("LatestVersion.txt");
            fos.getChannel().transferFrom(rbc, 0, 1 << 24);
            
        } catch (IOException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{
            
            // Open the file that is the first
            // command line parameter
            FileInputStream fstream = new FileInputStream("LatestVersion.txt");
            
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            
            //Read File Line By Line
            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
                // System.out.println (strLine);
                setUpdateVersion(strLine);
            }
            //Close the input stream
            in.close();
            
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        
        if(_Version.equals(getUpdateVersion())){
            updateStatus.setText("Up To Date.");
        }else{
            
            try {
                URL google = new URL("http://dl.dropbox.com/u/83362877/ConManager.jar");
                ReadableByteChannel rbc = Channels.newChannel(google.openStream());
                FileOutputStream fos = new FileOutputStream("ConManager.jar");
                fos.getChannel().transferFrom(rbc, 0, 1 << 24);
                Timer x = new Timer(delay5s, CloseTask);
                x.start();
                Display("Updated! Closing Program.");
                
            } catch (IOException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_cfuButtonActionPerformed

    private void pVUPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pVUPButtonActionPerformed
        try {
            if(!pVUPBox.getText().equals("") && !pVUPSpaceBox.getText().equals("")){
                if(getActiveService().hasUser(getActiveToken(), pVUPBox.getText())){
                    boolean spaceCheck=false;
                    String spaceKey="";
                    RemoteSpaceSummary[] scTest = getActiveService().getSpaces(getActiveToken());
                    
                    for(int i=0;i<scTest.length;i++){
                        if(scTest[i].getName().equals(pVUPSpaceBox.getText())){
                            spaceCheck=true;
                            spaceKey=scTest[i].getKey();
                        }
                    }
                    if(spaceCheck && !spaceKey.equals("")){
                        String[] x = getActiveService().getPermissionsForUser(getActiveToken(), spaceKey, pVUPBox.getText());
                        pVUPList.setListData(x);
                        for(int z=0;z<x.length;z++){
                            System.out.println(x[z]);
                        }
                        Display("Permissions Found.");
                    }else{
                        Display("Error With Entered Space/Permissions Not Found.");
                    }
                }else{
                    Display("User Doesn't Exist.");
                }
            }else{
                Display("A Required Field Was Left Blank");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_pVUPButtonActionPerformed

    private void pSpaceSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pSpaceSelectorActionPerformed
        String x = (String) pSpaceSelector.getSelectedItem();
        pVUPSpaceBox.setText(x);
}//GEN-LAST:event_pSpaceSelectorActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
        // TODO add your handling code here:
        if(!pwIDBox.getText().equals("")){
            if(userExists(pwIDBox.getText())){
                try {
                    getActiveService().changeUserPassword(getActiveToken(), pwIDBox.getText(), pcPasswordBox.getText());
                    Display("Password Changed.");
                } catch (java.rmi.RemoteException ex) {
                    Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                Display("User Not Found.");
            }
        }else{
            Display("A Required Field Was Left Blank.");
        }
}//GEN-LAST:event_jButton2ActionPerformed

    private void adDeleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adDeleteButtonActionPerformed
        
        // TODO add your handling code here:
        if(!adUserBox.getText().equals("")){
            if(userExists(adUserBox.getText())){
                try {
                    getActiveService().removeUser(getActiveToken(), adUserBox.getText());
                    Display("User Deleted.");
                } catch (java.rmi.RemoteException ex) {
                    Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                Display("User Not Found");
            }}else{
            Display("A Required Field Was Left Blank");
            }
    }//GEN-LAST:event_adDeleteButtonActionPerformed

    private void edituserButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edituserButtonActionPerformed
        try {
            
            String username = searchuserBox.getText();
            
            if(getActiveService().hasUser(getActiveToken(), username)){
                RemoteUser x = getActiveService().getUser(getActiveToken(), username);
                x.setName(nameBox.getText());
                x.setEmail(emailBox.getText());
                x.setFullname(fullnameBox.getText());
                getActiveService().editUser(getActiveToken(), x);
                Display("Your Changes Were Committed.");
            }else{
                Display("User Not Found.");
            }
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_edituserButtonActionPerformed

    private void searchuserButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchuserButtonActionPerformed
        try {
            if(!searchuserBox.getText().equals("")){
                // TODO add your handling code here:
                nameBox.setEnabled(true);
                fullnameBox.setEnabled(true);
                emailBox.setEnabled(true);
                urltouserBox.setEnabled(true);
                
                
                String username = searchuserBox.getText();
                if(getActiveService().hasUser(getActiveToken(), username)){
                    RemoteUser x = getActiveService().getUser(getActiveToken(), username);
                    nameBox.setText(x.getName());
                    fullnameBox.setText(x.getFullname());
                    emailBox.setText(x.getEmail());
                    urltouserBox.setText(x.getUrl());
                }else{
                    
                    Display("User Not Found.");
                }
            }else{
                Display("A Required Field Was Left Blank.");
            }
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_searchuserButtonActionPerformed

    private void cangButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cangButtonActionPerformed
        if(!cangNameBox.getText().equals("")){
            try {
                if(!getActiveService().hasGroup(getActiveToken(), cangNameBox.getText())) {
                    getActiveService().addGroup(getActiveToken(), cangNameBox.getText());
                    Display("Group Created.");
                    refreshGroupSelector();
                }else{
                    Display("Group Already Exists.");
                }
            } catch (java.rmi.RemoteException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            Display("A Required Field Was Left Blank.");
        }
}//GEN-LAST:event_cangButtonActionPerformed

    private void dagButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dagButtonActionPerformed
        try {
            if(!dagBox.getText().equals("") && !dagReplaceBox.getText().equals("")){
                if(getActiveService().hasGroup(getActiveToken(), dagBox.getText())) {
                    getActiveService().removeGroup(getActiveToken(),dagBox.getText(), dagReplaceBox.getText());
                    Display("Group Removed.");
                    refreshGroupSelector();
                }else{
                    Display("Group Doesn't Exist.");
                }
                
            }else{
                Display("A Required Field Was Left Blank");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_dagButtonActionPerformed

    private void vgButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vgButtonActionPerformed
        try {
            if(!vgIDBox.getText().equals("")){
                if(userExists(vgIDBox.getText())){
                    String[] x = getActiveService().getUserGroups(getActiveToken(), vgIDBox.getText());
                    vgList.setListData(x);
                    Display("User Found.");
                }else{
                    Display("User Not Found.");
                }
                
            }else{
                Display("A Required Field Was Left Blank.");
            }
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_vgButtonActionPerformed

    private void rufgButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rufgButtonActionPerformed
        try {
            // TODO add your handling code here:
            if(!rufgIDBox.getText().equals("")){
                if(getActiveService().hasUser(getActiveToken(), rufgIDBox.getText())){
                    // rufgStatus.setText("User Found.");
                    if(getActiveService().hasGroup(getActiveToken(), rufgGroupBox.getText())){
                        Display("User Removed.");
                        getActiveService().removeUserFromGroup(getActiveToken(), rufgIDBox.getText(), rufgGroupBox.getText());
                    }else{
                        Display("Group Doesn't Exist.");
                    }
                }else{
                    Display("User Not Found.");
                }
                
            }else{
                Display("A Required Field Was Left Blank.");
            }
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_rufgButtonActionPerformed

    private void autgButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autgButtonActionPerformed
        try {
            // TODO add your handling code here:
            if(!autgIDBox.getText().equals("")){
                if(getActiveService().hasUser(getActiveToken(), autgIDBox.getText())){
                    //autgStatus.setText("User Found.");
                    if(getActiveService().hasGroup(getActiveToken(), autgGroupBox.getText())){
                        Display("User Added.");
                        getActiveService().addUserToGroup(getActiveToken(), autgIDBox.getText(), autgGroupBox.getText());
                    }else{
                        Display("Group Doesn't Exist.");
                    }
                }else{
                    Display("User Not Found.");
                }
            }else{
                Display("A Required Field Was Left Blank.");
            }
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_autgButtonActionPerformed

    private void groupComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_groupComboBoxActionPerformed
        // TODO add your handling code here:
        String x = (String) groupComboBox.getSelectedItem();
        autgGroupBox.setText(x);
        rufgGroupBox.setText(x);
        dagBox.setText(x);
}//GEN-LAST:event_groupComboBoxActionPerformed

    private void acNotifyUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_acNotifyUserActionPerformed
        // TODO add your handling code here:
        notifyUser=!notifyUser;
}//GEN-LAST:event_acNotifyUserActionPerformed

    private void automakeCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_automakeCheckBoxActionPerformed
        // TODO add your handling code here:
        autoCreate=!autoCreate;
        acIDBox.setEnabled(!acIDBox.isEnabled());
        acPasswordBox.setEnabled(!acPasswordBox.isEnabled());
}//GEN-LAST:event_automakeCheckBoxActionPerformed

    private void notificationsCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_notificationsCheckActionPerformed
        // TODO add your handling code here:
        notificationCheck=!notificationCheck;
        acNotifyUser.setEnabled(!acNotifyUser.isEnabled());
        acSubjectBox.setEnabled(!acSubjectBox.isEnabled());
        acMessageBox.setEnabled(!acMessageBox.isEnabled());
        acNotifyUser.setSelected(!acNotifyUser.isSelected());
        acFWEmail1Box.setText("");
        acFWEmail1Box.setEnabled(!acFWEmail1Box.isEnabled());
        acFWEmail2Box.setEnabled(!acFWEmail2Box.isEnabled());
        acFWEmail3Box.setEnabled(!acFWEmail3Box.isEnabled());
        senderPasswordBox.setEnabled(!senderPasswordBox.isEnabled());
        senderEmailBox.setEnabled(!senderEmailBox.isEnabled());
        senderSMTPBox.setEnabled(!senderSMTPBox.isEnabled());
        senderPortBox.setEnabled(!senderPortBox.isEnabled());
}//GEN-LAST:event_notificationsCheckActionPerformed

    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed
        // TODO add your handling code here:
        boolean userAlreadyExists=false;
        
        String fullname = acFullnameBox.getText();
        String email = acEmailBox.getText();
        String userpassword = acPasswordBox.getText();
        
        
        String forward1 = acFWEmail1Box.getText();
        String forward2 = acFWEmail2Box.getText();
        String forward3 = acFWEmail3Box.getText();
        
        String senderEmail = senderEmailBox.getText();
        
        char[] pass = senderPasswordBox.getPassword();
        StringBuilder x = new StringBuilder();
        String senderPassword="";
        for(int i=0;i<pass.length;i++){x.append(pass, i, 1);}
        senderPassword=x.toString();
        
        String SMTPServer = senderSMTPBox.getText();
        String PortNumber = senderPortBox.getText();
        
        //Create user with custom id and pass or auto
        RemoteUser person = new RemoteUser();
        
        person.setFullname(fullname);
        person.setEmail(email);
        
        String autoID;
        autoID = fullname.substring(0,1);
        autoID+= fullname.substring(fullname.indexOf(' ')+1);
        autoID = autoID.toLowerCase();
        autoID = autoID.replace(" ", "");
        autoID = autoID.replaceAll("[-'`]", "");
        
        String autoPASS;
        autoPASS=generatePassword();
        
        //parse user groups
        String[] groups = acUserGroupsBox.getText().split(",");
        if(groups.length!=0){
            for(int i=0;i<groups.length;i++){
                groups[i]=groups[i].replace(" ", "");
            }
        }
        
        if(autoCreate){
            try {
                //set id
                person.setName(autoID);
                if(!(getActiveService().hasUser(getActiveToken(), person.getName()))) {
                    
                    getActiveService().addUser(getActiveToken(),person, autoPASS);
                    //getActiveService().addUserToGroup(getActiveToken(), person.getName(), "confluence-users");
                    //getActiveService().addUserToGroup(getActiveToken(), person.getName(), "needs-to-agree");
                    for(int i=0;i<groups.length;i++){
                        getActiveService().addUserToGroup(getActiveToken(), person.getName(), groups[i]);
                        //System.out.println(groups[i]);
                    }
                    Display("Account Created, with Auto ID.");
                }else{
                    Display("User Already Exists.");
                    userAlreadyExists=true;
                }
            } catch (java.rmi.RemoteException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        }else{
            try {
                //set id
                person.setName(acIDBox.getText());
                if(!(getActiveService().hasUser(getActiveToken(), person.getName()))) {
                    
                    getActiveService().addUser(getActiveToken(),person, userpassword);
                    //getActiveService().addUserToGroup(getActiveToken(), person.getName(), "confluence-users");
                    //getActiveService().addUserToGroup(getActiveToken(), person.getName(), "needs-to-agree");
                    
                    for(int i=0;i<groups.length;i++){
                        getActiveService().addUserToGroup(getActiveToken(), person.getName(), groups[i]);
                    }
                    Display("Account Created, With Custom ID.");
                }else{
                    Display("User Already Exists.");
                    userAlreadyExists=true;
                }
            } catch (java.rmi.RemoteException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(!notificationCheck && userAlreadyExists==false){
            
            EMAIL_TEMPLATE = acMessageBox.getText();
            EMAIL_SUBJECT = acSubjectBox.getText();
            
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", SMTPServer);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            Message m;
            Session s = Session.getDefaultInstance(props);
            m = new MimeMessage(s);
            
            if(notifyUser==false && acFWEmail1Box.getText().length()==0 && acFWEmail2Box.getText().length()==0 && acFWEmail3Box.getText().length()==0){
                
            }else{
                try {
                    if(notifyUser){
                        try {
                            m.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
                        } catch (MessagingException ex) {
                            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if(acFWEmail1Box.getText().length()!=0){
                        try {
                            m.addRecipient(Message.RecipientType.CC, new InternetAddress(forward1));
                        } catch (MessagingException ex) {
                            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if(acFWEmail2Box.getText().length()!=0){
                        try {
                            m.addRecipient(Message.RecipientType.CC, new InternetAddress(forward2));
                        } catch (MessagingException ex) {
                            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if(acFWEmail3Box.getText().length()!=0){
                        try {
                            m.addRecipient(Message.RecipientType.CC, new InternetAddress(forward3));
                        } catch (MessagingException ex) {
                            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    
                    m.setSubject(EMAIL_SUBJECT);
                    
                    if(autoCreate){
                        m.setText(String.format(EMAIL_TEMPLATE, person.getFullname(), person.getName(), autoPASS));
                        Transport t = s.getTransport();
                        t.connect(SMTPServer, Integer.parseInt(PortNumber), senderEmail, senderPassword);
                        t.sendMessage(m,  m.getAllRecipients());
                        t.close();
                        System.out.println("email sent with auto");
                    }else {
                        m.setText(String.format(EMAIL_TEMPLATE, person.getFullname(), person.getName(), userpassword));
                        Transport t = s.getTransport();
                        t.connect(SMTPServer, Integer.parseInt(PortNumber), senderEmail, senderPassword);
                        t.sendMessage(m,  m.getAllRecipients());
                        t.close();
                        System.out.println("email sent with custom");
                    }
                }
                //Add the user groups
                catch (MessagingException ex) {
                    Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        }
}//GEN-LAST:event_createButtonActionPerformed

    private void pmAddSpaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pmAddSpaceActionPerformed
        //Check to see if space already exists in current parameters
        boolean doubleCheck=false;
        for(int i=0;i<pmSpaceArea.size();i++){
            if(pmSpaceArea.get(i)!=null){
                if(spaceSummary[pmSpaceSelector.getSelectedIndex()].getName().equals(pmSpaceArea.get(i).getName())){
                    doubleCheck=true;
                }
            }
        }
        //If space is not already in search area
        if(doubleCheck==false){
            pmSpaceArea.add(spaceSummary[pmSpaceSelector.getSelectedIndex()]);
            setpmSpaces();
        }else{
            Display("Space Is Already Exists.");
        }
}//GEN-LAST:event_pmAddSpaceActionPerformed

    private void pmAddAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pmAddAllActionPerformed
        if(!(pmSpaceArea.size()==spaceSummary.length)){
            //Remove all spaces in space area
            pmSpaceArea = new ArrayList<RemoteSpaceSummary>();
            
            //add all spaces
            for(int i=0;i<spaceSummary.length;i++){
                pmSpaceArea.add(spaceSummary[i]);
            }
            
            //update display
            setpmSpaces();
        }else{
            Display("All Spaces Already Exist");
        }
}//GEN-LAST:event_pmAddAllActionPerformed

    private void pmRemoveSelectedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pmRemoveSelectedActionPerformed
        // TODO add your handling code here:
        if(pmSpaces.getSelectedIndex()!=-1){
            pmSpaceArea.remove(pmSpaces.getSelectedIndex());
            setpmSpaces();
        }else{
            Display("No Space Selected.");
        }
        
    }//GEN-LAST:event_pmRemoveSelectedActionPerformed

    private void pmRemoveAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pmRemoveAllActionPerformed
        pmSpaceArea = new ArrayList<RemoteSpaceSummary>();
        setpmSpaces();
}//GEN-LAST:event_pmRemoveAllActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        try {
            java.awt.Desktop.getDesktop().browse(java.net.URI.create(myDomain+"/admin/permissions/globalpermissions.action"));
        } catch (IOException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void pmPerformButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pmPerformButtonActionPerformed
        
        
        //Set Progress bar & disable features that could interfere with search
        pmPerformButton.setEnabled(false);
        pmProgressBar.setValue(0);
        pmProgressBar.setMaximum(pmSpaceArea.size());
        pmEntityBox.setEnabled(false);
        
        //Add Permissions For A User Or Group
        if(pmOperationSelector.getSelectedIndex()==0){
            try {
                if(pmEntityCheck() && pmPermCheck() && pmSpaceCheck()){
                    class SearchThread extends SwingWorker<String[], Object> {
                        @Override
                        public String[] doInBackground() throws java.rmi.RemoteException {
                            String[] perms = pmBuildPermissionVector();
                            //used for removing all permissions that are currently set
                            String[] allPerms = permissionsSummary;
                            //Set Spaces Specified
                            //Set Spaces Modified
                            //Set EntityName
                            //Set Perm list
                            pmEntityResultsBox.setText(pmEntityBox.getText());
                            pmSpacesSpecifiedBox.setText(String.valueOf(pmSpaceArea.size()));
                            pmSpacesModifiedBox.setText(String.valueOf(0));
                            pmPermList.setListData(perms);
                            pmOperationBox.setText("Set Permissions");
                            for(int i=0;i<pmSpaceArea.size();i++){
                                //There is no function to remove permissions by vector, you have to do it 1 at a time Lol
                                for(int z=0;z<allPerms.length;z++){
                                    getActiveService().removePermissionFromSpace(getActiveToken(), allPerms[z], pmEntityBox.getText(), pmSpaceArea.get(i).getKey());
                                }
                                getActiveService().addPermissionsToSpace(getActiveToken(), perms, pmEntityBox.getText(), pmSpaceArea.get(i).getKey());
                                pmSpacesModifiedBox.setText(String.valueOf(Integer.parseInt(pmSpacesModifiedBox.getText())+1));
                                pmProgressBar.setValue(pmProgressBar.getValue()+1);
                            }
                            return perms;
                        }
                        @Override
                        protected void done() {
                            try {
                                //worker thread completed.
                                Display("Operation Complete.");
                                pmPerformButton.setEnabled(true);
                                pmEntityBox.setEnabled(true);
                            } catch (Exception ignore) {
                            }
                        }
                    }
                    (new SearchThread()).execute();
                }else{
                    //Set Progress For Broken Request & Re-enable
                    pmOperationBox.setText("Set Permissions - FAILED.");
                    pmPerformButton.setEnabled(true);
                    pmEntityBox.setEnabled(true);
                    String[] perms = pmBuildPermissionVector();
                    pmEntityResultsBox.setText(pmEntityBox.getText());
                    pmSpacesSpecifiedBox.setText(String.valueOf(pmSpaceArea.size()));
                    pmSpacesModifiedBox.setText(String.valueOf("0"));
                    pmPermList.setListData(perms);
                }
                
            } catch (RemoteException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        //Remove Permissions
        if(pmOperationSelector.getSelectedIndex()==1){
            try {
                if(pmEntityCheck() && pmPermCheck() && pmSpaceCheck()){
                    class SearchThread extends SwingWorker<String[], Object> {
                        @Override
                        public String[] doInBackground() throws java.rmi.RemoteException {
                            String[] perms = pmBuildPermissionVector();
                            String[] allPerms = permissionsSummary;
                            //Set Spaces Specified
                            //Set Spaces Modified
                            //Set EntityName
                            //Set Perm list
                            pmEntityResultsBox.setText(pmEntityBox.getText());
                            pmSpacesSpecifiedBox.setText(String.valueOf(pmSpaceArea.size()));
                            pmSpacesModifiedBox.setText(String.valueOf(0));
                            pmPermList.setListData(perms);
                            pmOperationBox.setText("Remove Permissions.");
                            //Traverse through specified spaces and then remove each of the permissions.
                            for(int i=0;i<pmSpaceArea.size();i++){
                                for(int z=0;z<perms.length;z++){
                                    getActiveService().removePermissionFromSpace(getActiveToken(), perms[z], pmEntityBox.getText(), pmSpaceArea.get(i).getKey());
                                }
                                pmSpacesModifiedBox.setText(String.valueOf(Integer.parseInt(pmSpacesModifiedBox.getText())+1));
                                pmProgressBar.setValue(pmProgressBar.getValue()+1);
                            }
                            return allPerms;
                        }
                        @Override
                        protected void done() {
                            try {
                                //worker thread completed.
                                Display("Operation Complete.");
                                pmPerformButton.setEnabled(true);
                                pmEntityBox.setEnabled(true);
                            } catch (Exception ignore) {
                            }
                        }
                    }
                    (new SearchThread()).execute();
                }else{
                    //Set Progress For Broken Request & Re-enable
                    pmOperationBox.setText("Remove Permissions - FAILED.");
                    pmPerformButton.setEnabled(true);
                    pmEntityBox.setEnabled(true);
                    pmEntityResultsBox.setText(pmEntityBox.getText());
                    pmSpacesSpecifiedBox.setText(String.valueOf(pmSpaceArea.size()));
                    pmSpacesModifiedBox.setText(String.valueOf("0"));
                    pmPermList.setListData(permissionsSummary);
                }
            } catch (RemoteException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        //Set Anon Perms
        if(pmOperationSelector.getSelectedIndex()==2){
            if(pmPermCheck() && pmSpaceCheck()){
                class SearchThread extends SwingWorker<String[], Object> {
                    @Override
                    public String[] doInBackground() throws java.rmi.RemoteException {
                        String[] perms = pmBuildPermissionVector();
                        String[] allPerms = permissionsSummary;
                        //Set Spaces Specified
                        //Set Spaces Modified
                        //Set EntityName
                        //Set Perm list
                        pmEntityResultsBox.setText(pmEntityBox.getText());
                        pmSpacesSpecifiedBox.setText(String.valueOf(pmSpaceArea.size()));
                        pmSpacesModifiedBox.setText(String.valueOf(0));
                        pmPermList.setListData(perms);
                        pmOperationBox.setText("Set Anonymous Permissions.");
                        
                        //Traverse through specified spaces and set the specified permissions
                        for(int i=0;i<pmSpaceArea.size();i++){
                            for(int z=0;z<allPerms.length;z++){
                                getActiveService().removeAnonymousPermissionFromSpace(getActiveToken(), allPerms[z], pmSpaceArea.get(i).getKey());
                            }
                            getActiveService().addAnonymousPermissionsToSpace(getActiveToken(), perms, pmSpaceArea.get(i).getKey());
                            pmSpacesModifiedBox.setText(String.valueOf(Integer.parseInt(pmSpacesModifiedBox.getText())+1));
                            pmProgressBar.setValue(pmProgressBar.getValue()+1);
                        }
                        return allPerms;
                    }
                    @Override
                    protected void done() {
                        try {
                            //worker thread completed.
                            Display("Operation Complete.");
                            pmPerformButton.setEnabled(true);
                            pmEntityBox.setEnabled(true);
                        } catch (Exception ignore) {
                        }
                    }
                }
                (new SearchThread()).execute();
            }else{
                //Set Progress For Broken Request & Re-enable
                pmOperationBox.setText("Set Anonymous Permissions - FAILED.");
                pmPerformButton.setEnabled(true);
                pmEntityBox.setEnabled(true);
                pmEntityResultsBox.setText(pmEntityBox.getText());
                pmSpacesSpecifiedBox.setText(String.valueOf(pmSpaceArea.size()));
                pmSpacesModifiedBox.setText(String.valueOf("0"));
                pmPermList.setListData(permissionsSummary);
            }
        }
        
        //Remove Anon Perms
        if(pmOperationSelector.getSelectedIndex()==3){
            if(pmPermCheck() && pmSpaceCheck()){
                class SearchThread extends SwingWorker<String[], Object> {
                    @Override
                    public String[] doInBackground() throws java.rmi.RemoteException {
                        String[] perms = pmBuildPermissionVector();
                        String[] allPerms = permissionsSummary;
                        //Set Spaces Specified
                        //Set Spaces Modified
                        //Set EntityName
                        //Set Perm list
                        pmEntityResultsBox.setText(pmEntityBox.getText());
                        pmSpacesSpecifiedBox.setText(String.valueOf(pmSpaceArea.size()));
                        pmSpacesModifiedBox.setText(String.valueOf(0));
                        pmPermList.setListData(perms);
                        pmOperationBox.setText("Remove Anonymous Permissions.");
                        
                        //Traverse through specified spaces and remove the specified permissions
                        for(int i=0;i<pmSpaceArea.size();i++){
                            for(int z=0;z<perms.length;z++){
                                getActiveService().removeAnonymousPermissionFromSpace(getActiveToken(), perms[z], pmSpaceArea.get(i).getKey());
                            }
                            pmSpacesModifiedBox.setText(String.valueOf(Integer.parseInt(pmSpacesModifiedBox.getText())+1));
                            pmProgressBar.setValue(pmProgressBar.getValue()+1);
                        }
                        return allPerms;
                    }
                    @Override
                    protected void done() {
                        try {
                            //worker thread completed.
                            Display("Operation Complete.");
                            pmPerformButton.setEnabled(true);
                            pmEntityBox.setEnabled(true);
                        } catch (Exception ignore) {
                        }
                    }
                }
                (new SearchThread()).execute();
            }else{
                //Set Progress For Broken Request & Re-enable
                pmOperationBox.setText("Remove Anonymous Permissions - FAILED.");
                pmPerformButton.setEnabled(true);
                pmEntityBox.setEnabled(true);
                pmEntityResultsBox.setText(pmEntityBox.getText());
                pmSpacesSpecifiedBox.setText(String.valueOf(pmSpaceArea.size()));
                pmSpacesModifiedBox.setText(String.valueOf("0"));
                pmPermList.setListData(permissionsSummary);
            }
        }
}//GEN-LAST:event_pmPerformButtonActionPerformed

    private void pmOperationSelectorPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_pmOperationSelectorPropertyChange
        if(evt.getOldValue()!=null){
            //System.out.println((evt.getOldValue().toString()));
            if(evt.getOldValue().toString().equalsIgnoreCase("PRESSED")){
                String req="";
                
                if(pmOperationSelector.getSelectedIndex()==0){
                    req=            "This operation adds the selected permissions, for a user/group you specified, in the spaces you specified.\n" +
                            "-Name Box is filled out and Type Selector is set.\n" +
                            "-Atleast 1 permsission box is checked.\n" +
                            "-Atleast 1 space is specified.\n";
                }
                if(pmOperationSelector.getSelectedIndex()==1){
                    req=            "This operation removes the selected permissions, in the specified spaces.\n" +
                            "-Name Box is filled out, and Type Selector is set.\n" +
                            "-Atleast 1 permission box is checked.\n" +
                            "-Atleast 1 space is specified.\n";
                }
                if(pmOperationSelector.getSelectedIndex()==2){
                    req=         "This operation adds the selected permissions, to the selected spaces, for Anonymous Access.\n" +
                            "-Name Box & Type selector are ignored.\n" +
                            "-Atleast 1 permission box is checked.\n" +
                            "-Atleast 1 space is added.\n";
                }
                if(pmOperationSelector.getSelectedIndex()==3){
                    req=            "This operation removes ALL permissions, from ALL spaces for Anonymous Access.\n" +
                            "-Name box & Type selector are ignored.\n" +
                            "-All checked permissions are ignored.\n" +
                            "-All Selected Spaces Are Ignored.\n";
                }
                pmReqs.setText(req);
            }
        }
}//GEN-LAST:event_pmOperationSelectorPropertyChange

    private void pmSelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pmSelectAllActionPerformed
        perm1.setSelected(pmSelectAll.isSelected());
        perm2.setSelected(pmSelectAll.isSelected());
        perm3.setSelected(pmSelectAll.isSelected());
        perm4.setSelected(pmSelectAll.isSelected());
        perm5.setSelected(pmSelectAll.isSelected());
        perm6.setSelected(pmSelectAll.isSelected());
        perm7.setSelected(pmSelectAll.isSelected());
        perm8.setSelected(pmSelectAll.isSelected());
        perm9.setSelected(pmSelectAll.isSelected());
        perm10.setSelected(pmSelectAll.isSelected());
        perm11.setSelected(pmSelectAll.isSelected());
        perm12.setSelected(pmSelectAll.isSelected());
        perm13.setSelected(pmSelectAll.isSelected());
        perm14.setSelected(pmSelectAll.isSelected());
}//GEN-LAST:event_pmSelectAllActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if(resultsList.getSelectedIndex()!=-1){
            //1. Create the frame.
            frametest = new ConEditEditor(getActiveService(), getActiveToken(), resultsData.get(resultsList.getSelectedIndex()));
            //2. Optional: What happens when the frame closes?
            frametest.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            //3. Create components and put them in the frame.
            //...create emptyLabel...
            //frame.getContentPane().add();
            //4. Size the frame.
            frametest.pack();
            //[1045, 683]
            frametest.setBounds(50,50,1050,690);
            //5. Show it.
            frametest.setVisible(true);
        }
}//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            java.awt.Desktop.getDesktop().browse(java.net.URI.create(resultsData.get(resultsList.getSelectedIndex()).getUrl()));
        } catch (IOException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_jButton1ActionPerformed

    private void clearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearButtonActionPerformed
        // TODO add your handling code here:
        
        for(int i=0;i<searchAreaList.getComponentCount();i++){
            searchAreaList.remove(i);
        }
        
        searchDataP = new ArrayList<RemotePageSummary>();
        searchDataS = new ArrayList<RemoteSpaceSummary>();
        searchAreaList.setListData(setSearchAreaDisplaySF());
}//GEN-LAST:event_clearButtonActionPerformed

    private void addPageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addPageButtonActionPerformed
        // TODO add your handling code here:
        boolean test = false;
        boolean test2 =false;
        
        //check if page is already in area
        for(int i=0;i<searchDataP.size();i++){
            if(searchDataP.get(i).getTitle().equalsIgnoreCase((String)pageSelector.getSelectedItem())){
                test=true;
                advOptionsStatus.setText("Page Already In Search Area.");
            }
        }
        //check if the pages, space parent is in the area
        try {
            //true if parent
            test2=checkIfPageHasParent(current_pageSummary[pageSelector.getSelectedIndex()], searchDataS);
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //if checks pass, add the page to the search area & search D.S.
        if(test==false && test2==false) {
            searchDataP.add(current_pageSummary[pageSelector.getSelectedIndex()]);
            setSearchAreaDisplaySF();
            advOptionsStatus.setText("Page Added.");
        }else{
            
        }
    }//GEN-LAST:event_addPageButtonActionPerformed

    private void addSpaceButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSpaceButtonActionPerformed
        //true = don't add
        boolean test = false;
        
        //check if space is already in search area
        for(int i=0;i<searchDataS.size();i++){
            if(searchDataS.get(i).getName().equalsIgnoreCase(spaceSummary[spaceSelector.getSelectedIndex()].getName())){
                test=true;
                advOptionsStatus.setText("Spaced Already Exists.");
            }
        }
        
        //System.out.println("Already Exists");
        
        if(test==false){
            try {
                //check if the search area contains any pages that belong to the new added space, and remove them.
                searchDataS.add(spaceSummary[spaceSelector.getSelectedIndex()]);
                test=checkIfSpaceHasChildren(spaceSummary[spaceSelector.getSelectedIndex()], searchDataP);
                advOptionsStatus.setText("Space Added.");
                consumeCheck=true;
                setSearchAreaDisplaySF();
            } catch (java.rmi.RemoteException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        //add space to search area and proper D.S.
        if(!test && !consumeCheck){
            //System.out.println("added normaly");
            searchDataS.add(spaceSummary[spaceSelector.getSelectedIndex()]);
            advOptionsStatus.setText("Space Added.");
            setSearchAreaDisplaySF();
        }
        
        consumeCheck=false;
    }//GEN-LAST:event_addSpaceButtonActionPerformed

    private void advOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_advOptionsActionPerformed
        try {
            // TODO add your handling code here:
            advOptionsEnabler=!advOptionsEnabler;
            //Enable Buttons&Selectors
            
            spaceSelector.setEnabled(!spaceSelector.isEnabled());
            pageSelector.setEnabled(!pageSelector.isEnabled());
            ssLabel.setEnabled(!ssLabel.isEnabled());
            psLabel.setEnabled(!psLabel.isEnabled());
            saLabel.setEnabled(!saLabel.isEnabled());
            addSpaceButton.setEnabled(!addSpaceButton.isEnabled());
            addPageButton.setEnabled(!addPageButton.isEnabled());
            searchAreaList.setEnabled(searchAreaList.isEnabled()); 
            
            //Create Space Selector
            spaceSelector.removeAllItems();
            for(int i=0;i<spaceSummary.length;i++){
                spaceSelector.addItem(spaceSummary[i].getName());
                //System.out.println(spaceSummary[i].getName()+":"+spaceSummary[i].getKey());
            }
            searchAreaList.removeAll();
            RemotePageSummary[] data = getActiveService().getPages(getActiveToken(), spaceSummary[spaceSelector.getSelectedIndex()].getKey());
            current_pageSummary = getActiveService().getPages(getActiveToken(), spaceSummary[0].getKey());
            pageSelector.removeAllItems();
            
            for(int z=0;z<data.length;z++){
                pageSelector.addItem(data[z].getTitle());
            }
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_advOptionsActionPerformed

    private void searchandrButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchandrButtonActionPerformed
        //reset bar and #
        searchProgressBar.setValue(0);
        searchDataFinal = new ArrayList<RemotePage>();
        resultsData = new ArrayList<RemotePage>();
        numberoftimesBox.setText("0");
        if(searchBox.getText()!=""){
            if(!advOptionsEnabler){
                
                searchandrButton.setEnabled(false);
                sandrButton.setEnabled(false);
                resultsList.setListData(new String[0]);
                
                class SearchThread extends SwingWorker<String[], Object> {
                    @Override
                    public String[] doInBackground() throws java.rmi.RemoteException {
                        
                        RemoteSpaceSummary[] spaceArray =  getActiveService().getSpaces(getActiveToken());
                        
                        numberoftimesBox.setText("0");
                        searchProgressBar.setValue(0);
                        searchProgressBar.setMaximum(spaceArray.length);
                        for (int i=0;i<spaceArray.length;i++) {
                            searchProgressBar.setValue(i);
                            RemotePageSummary[] pageArray = getActiveService().getPages(getActiveToken(), spaceArray[i].getKey());
                            
                            for (RemotePageSummary pageInfo : pageArray) {
                                
                                RemotePage page = getActiveService().getPage(getActiveToken(), pageInfo.getId());
                                String content=page.getContent();
                                
                                if(content.contains(searchBox.getText())){
                                    resultsData.add(page);
                                    numberoftimesBox.setText(String.valueOf(Integer.parseInt(numberoftimesBox.getText())+1));
                                }
                            }
                            currentStatus.setText(searchProgressBar.getString()+" - "+"Searching " +spaceArray[i].getName()+" space.");
                        }
                        return setResultsListDisplaySF();
                    }
                    @Override
                    protected void done() {
                        try {
                            currentStatus.setText("Search Complete.");
                            resultsList.setListData(this.get());
                            searchandrButton.setEnabled(true);
                            sandrButton.setEnabled(true);
                        } catch (Exception ignore) {
                        }
                    }
                }
                (new SearchThread()).execute();
            } else{
                
                //System.out.println("hit");
                searchandrButton.setEnabled(false);
                sandrButton.setEnabled(false);
                resultsList.setListData(new String[10]);
                
                
                class SearchThread2 extends SwingWorker<String[], Object> {
                    @Override
                    public String[] doInBackground() throws java.rmi.RemoteException {
                        numberoftimesBox.setText("0");
                        //add pages from spaces data structure
                        
                        searchProgressBar.setValue(0);
                        searchProgressBar.setMaximum(searchDataP.size());
                        for(int i=0;i<searchDataP.size();i++){
                            currentStatus.setText(searchProgressBar.getString()+" - "+ "Loading Pages...");
                            searchDataFinal.add(getActiveService().getPage(getActiveToken(), searchDataP.get(i).getId()));
                            searchProgressBar.setValue(searchProgressBar.getValue()+1);
                        }
                        //add spaces (get all pages from each space) from appropriate D.S.
                        //System.out.println("derp2");
                        
                        for(int z=0;z<searchDataS.size();z++){
                            RemotePageSummary[] temp = getActiveService().getPages(getActiveToken(), searchDataS.get(z).getKey());
                            
                            searchProgressBar.setMaximum(temp.length);
                            searchProgressBar.setValue(0);
                            
                            for(int q=0;q<temp.length;q++){
                                searchDataFinal.add(getActiveService().getPage(getActiveToken(), temp[q].getId()));
                                searchProgressBar.setValue(searchProgressBar.getValue()+1);
                                currentStatus.setText(searchProgressBar.getString()+" - "+"Loading " +searchDataS.get(z).getName()+" space.");
                            }
                            
                        }
                        //System.out.println("searchDataFinal is loaded & searching..adding results to resultsData");
                        //if sdf contains the text, add it to rd ds
                        //System.out.println(searchDataFinal.size());
                        searchProgressBar.setValue(0);
                        searchProgressBar.setMaximum(searchDataFinal.size());
                        for(int t=0;t<searchDataFinal.size();t++){
                            currentStatus.setText(searchProgressBar.getString()+" - "+"Searching.." + searchProgressBar.getString());
                            //System.out.println(t+"/"+searchDataFinal.size()+":"+searchDataFinal.get(t).getTitle());
                            if(searchDataFinal.get(t).getContent().contains(searchBox.getText())) {
                                numberoftimesBox.setText(String.valueOf(Integer.parseInt(numberoftimesBox.getText())+1));
                                resultsData.add(searchDataFinal.get(t));
                            }
                            searchProgressBar.setValue(searchProgressBar.getValue());
                        }
                        //translate rd ds to string array and display
                        return setResultsListDisplaySF();
                    }
                    @Override
                    protected void done() {
                        try {
                            currentStatus.setText("Search Complete.");
                            resultsList.setListData(this.get());
                            searchandrButton.setEnabled(true);
                            sandrButton.setEnabled(true);
                        } catch (Exception ignore) {
                        }
                    }
                }
                SearchThread2 customSearch = new SearchThread2();
                customSearch.execute();
            }
        }
}//GEN-LAST:event_searchandrButtonActionPerformed

    private void sandrButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sandrButtonActionPerformed
        // TODO add your handling code here:
        //sandrButton code
        if(!searchBox.getText().equals(replaceBox.getText())){
            searchDataFinal = new ArrayList<RemotePage>();
            resultsData = new ArrayList<RemotePage>();
            numberoftimesBox.setText("0");
            if(!reasonBox.getText().equalsIgnoreCase("Enter reason for change here") && !reasonBox.getText().equalsIgnoreCase("")){
                if(!advOptionsEnabler){
                    
                    searchandrButton.setEnabled(false);
                    sandrButton.setEnabled(false);
                    resultsList.setListData(new String[10]);
                    
                    class SearchThread3 extends SwingWorker<String[], Object>{
                        
                        @Override
                        protected String[] doInBackground() throws Exception {
                            searchProgressBar.setValue(0);
                            searchProgressBar.setMaximum(spaceSummary.length);
                            //go through all spaces and add to results data
                            for(int i=0;i<spaceSummary.length;i++){
                                
                                RemotePageSummary[] current = getActiveService().getPages(getActiveToken(), spaceSummary[i].getKey());
                                
                                currentStatus.setText(searchProgressBar.getString()+" - "+"Searching "+spaceSummary[i].getName());
                                
                                for(int z=0;z<current.length;z++){
                                    RemotePage temp = getActiveService().getPage(getActiveToken(), current[z].getId());
                                    if(temp.getContent().contains(searchBox.getText())){
                                        //found, add to DS, increment # of times, increment progress bar
                                        resultsData.add(temp);
                                        numberoftimesBox.setText(String.valueOf(Integer.parseInt(numberoftimesBox.getText())+1));
                                    }
                                    
                                }
                                searchProgressBar.setValue(searchProgressBar.getValue()+1);
                            }
                            //traverse results data and update
                            for(int t=0;t<resultsData.size();t++){
                                String content=resultsData.get(t).getContent();
                                String update=content.replace(searchBox.getText(), replaceBox.getText());
                                resultsData.get(t).setContent(update);
                                getActiveService().updatePage(getActiveToken(), resultsData.get(t), new RemotePageUpdateOptions(true, reasonBox.getText()));
                            }
                            return setResultsListDisplaySF();
                        }
                        @Override
                        protected void done() {
                            try {
                                Display("Complete.");
                                resultsList.setListData(this.get());
                                sandrButton.setEnabled(true);
                                searchandrButton.setEnabled(true);
                            } catch (Exception ignore){
                            }
                        }
                        
                    }
                    (new SearchThread3()).execute();
                    
                }else{
                    //Search and replace with advanced options enabled.
                    searchandrButton.setEnabled(false);
                    sandrButton.setEnabled(false);
                    resultsList.setListData(new String[10]);
                    
                    class SearchThread4 extends SwingWorker<String[], Object> {
                        @Override
                        public String[] doInBackground() throws java.rmi.RemoteException {
                            
                            String[] data;
                            numberoftimesBox.setText("0");
                            
                            //add pages from spaces data structure
                            searchProgressBar.setMaximum(searchDataP.size());
                            searchProgressBar.setValue(0);
                            for(int i=0;i<searchDataP.size();i++){
                                searchDataFinal.add(getActiveService().getPage(getActiveToken(), searchDataP.get(i).getId()));
                                currentStatus.setText(searchProgressBar.getString()+" - "+ "Loading Pages...");
                                searchProgressBar.setValue(searchProgressBar.getValue()+1);
                            }
                            //add spaces (get all pages from each space) from appropriate D.S.
                            //System.out.println("derp2");
                            searchProgressBar.setMaximum(searchDataS.size());
                            searchProgressBar.setValue(0);
                            for(int z=0;z<searchDataS.size();z++){
                                RemotePageSummary[] temp = getActiveService().getPages(getActiveToken(), searchDataS.get(z).getKey());
                                currentStatus.setText(searchProgressBar.getString()+" - "+"Loading " +searchDataS.get(z).getName()+" space.");
                                
                                for(int q=0;q<temp.length;q++){
                                    searchDataFinal.add(getActiveService().getPage(getActiveToken(), temp[q].getId()));
                                }
                                searchProgressBar.setValue(searchProgressBar.getValue()+1);
                                
                            }
                            
                            //System.out.println("searchDataFinal is loaded & searching..adding results to resultsData");
                            //if sdf contains the text, add it to rd ds
                            //System.out.println(searchDataFinal.size());
                            searchProgressBar.setValue(0);
                            searchProgressBar.setMaximum(searchDataFinal.size());
                            for(int t=0;t<searchDataFinal.size();t++) {
                                currentStatus.setText(searchProgressBar.getString()+" - "+"Searching.." + searchProgressBar.getString());
                                if(searchDataFinal.get(t).getContent().contains(searchBox.getText())) {
                                    numberoftimesBox.setText(String.valueOf(Integer.parseInt(numberoftimesBox.getText())+1));
                                    resultsData.add(searchDataFinal.get(t));
                                }
                                searchProgressBar.setValue(searchProgressBar.getValue()+1);
                            }
                            //traverse resultsdata and update all pages
                            for(int i=0;i<resultsData.size();i++){
                                String content=resultsData.get(i).getContent();
                                String update=content.replace(searchBox.getText(), replaceBox.getText());
                                resultsData.get(i).setContent(update);
                                getActiveService().updatePage(getActiveToken(), resultsData.get(i), new RemotePageUpdateOptions(true, reasonBox.getText()));
                            }
                            Display("Search & Replace Complete.");
                            return setResultsListDisplaySF();
                        }
                        @Override
                        protected void done() {
                            try {
                                resultsList.setListData(this.get());
                                sandrButton.setEnabled(true);
                                searchandrButton.setEnabled(true);
                            } catch (Exception ignore){
                            }
                        }
                    }
                    (new SearchThread4()).execute();
                }
            }else{
                Display("If you are going to edit (search & replace) you need to provide a reason or comment.");
            }
        }else{
            Display("Search Text And Replace Text Cannot Be The Same.");
        }
}//GEN-LAST:event_sandrButtonActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
        sandrButton.setEnabled(!sandrButton.isEnabled());
}//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed

        System.out.println(acMessageBox.getText());
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton6ActionPerformed

    private void seiExportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seiExportButtonActionPerformed
        
            seiExportButton.setEnabled(false); 
            /*
             String exportSpace(String token, String spaceKey, String exportType) - 
                 * exports a space and returns a String holding the URL for the download. 
                 * The export type argument indicates whether or not to export in XML or HTML format - use "TYPE_XML" or "TYPE_HTML" respectively. 
                 * Also, using "all" will select TYPE_XML.
             */
            seiExportBar.setValue(0);
            seiExportBar.setMaximum(100);
            final Timer timere;
            timere = new Timer(delay1s, ExportSpaceTask);
            timere.start();
        
         class SEISearchThread extends SwingWorker<String[], Object> {
         @Override
         public String[] doInBackground() throws java.rmi.RemoteException {
            try{
                
               String export = "TYPE_XML";
               String download = getActiveService().exportSpace(getActiveToken(), spaceSummary[seiSpaceSelector.getSelectedIndex()].getKey(), export);
               seiDownloadLink.setText(download);
                
           } catch (RemoteException ex) {
              Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
           }
           return (new String[1]);
           
        }
        @Override
        protected void done() {
            try {
                Display("Space Exported!");
                seiExportButton.setEnabled(true);
                seiDownload.setEnabled(true);
                timere.stop();
            } catch (Exception ignore) {
                timere.stop();
            }
        }
       }
         
     
     
     (new SEISearchThread()).execute();
    }//GEN-LAST:event_seiExportButtonActionPerformed

    private void seiDownloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seiDownloadActionPerformed
        try {
            if(!seiDownloadLink.getText().equals("")){
                java.awt.Desktop.getDesktop().browse(java.net.URI.create(seiDownloadLink.getText()));
            }
        } catch (IOException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_seiDownloadActionPerformed

    private void seiFileChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seiFileChooserActionPerformed
      if(evt.toString().contains("java.awt.event.ActionEvent[ACTION_PERFORMED,cmd=ApproveSelection,")){
 
      }
      if(evt.toString().equals("java.awt.event.ActionEvent[ACTION_PERFORMED,cmd=CancelSelection,")){
          
      }
      System.out.println(evt.toString());        
    }//GEN-LAST:event_seiFileChooserActionPerformed

    private void seiImportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seiImportButtonActionPerformed
          
        if(seiFileChooser.getSelectedFile()!=null){
                seiImportButton.setEnabled(false);
                seiImportBar.setValue(0);
                seiImportBar.setMaximum(100);
                seiFileChooser.setEnabled(false);
                final Timer timeImport;
                timeImport = new Timer(delay1s, ImportSpaceTask);
                timeImport.start();

                    class SEIImportThread extends SwingWorker<String[], Object> {
                    @Override
                    public String[] doInBackground() throws java.rmi.RemoteException {
                    try {

                         File test = seiFileChooser.getSelectedFile();
                         byte[] space = Files.readAllBytes(test.toPath());      
                         getActiveService().importSpace(getActiveToken(), space);

                        } catch (IOException ex) {
                            Display("Error.");
                            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
                        }  
                        return (new String[1]);
                    }
                    @Override
                    protected void done() {
                        try {
                            Display("Space Imported!");
                            seiImportButton.setEnabled(true);
                            timeImport.stop();
                            seiFileChooser.setEnabled(true);
                        } catch (Exception ignore) {
                            Display("Some Error Occured.");
                            timeImport.stop();
                            seiFileChooser.setEnabled(true);
                        }
                    }
                  }
                 (new SEIImportThread()).execute();
            }else{
              Display("No File Selected.");  
            }   
        
    }//GEN-LAST:event_seiImportButtonActionPerformed

    private void carClearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_carClearButtonActionPerformed
        for(int i=0;i<carSearchAreaList.getComponentCount();i++){
            carSearchAreaList.remove(i);
        }
        
        carSearchAreaP = new ArrayList<RemotePageSummary>();
        carSearchAreaS = new ArrayList<RemoteSpaceSummary>();
        carSearchAreaList.setListData(setSearchAreaDisplayCAR());
    }//GEN-LAST:event_carClearButtonActionPerformed

    private void carAddPageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_carAddPageButtonActionPerformed
        // TODO add your handling code here:
        boolean test = false;
        boolean test2 =false;
        
        //check if page is already in area
        for(int i=0;i<carSearchAreaP.size();i++){
            if(carSearchAreaP.get(i).getTitle().equalsIgnoreCase((String)carPageSelector.getSelectedItem())){
                test=true;
            }
        }
        //check if the pages, space parent is in the area
        try {
            //true if parent
            test2=checkIfPageHasParent(car_current_pageSummary[carPageSelector.getSelectedIndex()], carSearchAreaS);
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //if checks pass, add the page to the search area & search D.S.
        if(test==false && test2==false) {
            carSearchAreaP.add(car_current_pageSummary[carPageSelector.getSelectedIndex()]);
            setSearchAreaDisplayCAR();
        }else{}
    }//GEN-LAST:event_carAddPageButtonActionPerformed

    private void carAddSpaceButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_carAddSpaceButtonActionPerformed
        //true = don't add
        boolean test = false;
        //check if space is already in search area
        for(int i=0;i<carSearchAreaS.size();i++){
            if(carSearchAreaS.get(i).getName().equalsIgnoreCase(spaceSummary[carSpaceSelector.getSelectedIndex()].getName())){
                test=true;
                Display("Already In Operation Area.");
            }
        }
        
        //System.out.println("Already Exists");
        
        if(test==false){
            try {
                //check if the search area contains any pages that belong to the new added space, and remove them.
                carSearchAreaS.add(spaceSummary[carSpaceSelector.getSelectedIndex()]);
                test=checkIfSpaceHasChildren(spaceSummary[carSpaceSelector.getSelectedIndex()], carSearchAreaP);
                consumeCheck=true;
                setSearchAreaDisplayCAR();
            } catch (java.rmi.RemoteException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        //add space to search area and proper D.S.
        if(!test && !consumeCheck){
            //System.out.println("added normaly");
            carSearchAreaS.add(spaceSummary[carSpaceSelector.getSelectedIndex()]);
            setSearchAreaDisplayCAR();
        } 
        consumeCheck=false;
    }//GEN-LAST:event_carAddSpaceButtonActionPerformed

    private void carAddAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_carAddAllButtonActionPerformed
        carSearchAreaList.removeAll();
        carSearchAreaS = new ArrayList<RemoteSpaceSummary>();
        carSearchAreaP = new ArrayList<RemotePageSummary>();
        String[] data = new String[spaceSummary.length];
        for(int i=0;i<spaceSummary.length;i++){
            data[i]=spaceSummary[i].getName();
            carSearchAreaS.add(spaceSummary[i]);
        }
        carSearchAreaList.setListData(data);

        
        
    }//GEN-LAST:event_carAddAllButtonActionPerformed

    private void seiRelogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seiRelogActionPerformed
        try {
            setActiveService(new ConfluenceSoapServiceServiceLocator(myDomain).getConfluenceserviceV1());
            String id = loginBox.getText();
            char[] pass = passwordBox.getPassword();
            StringBuilder test = new StringBuilder();
            String password="";
            for(int i=0;i<pass.length;i++){test.append(pass, i, 1);}
            password=test.toString();
            setActiveToken(getActiveService().login(id, password));
            
        } catch (RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }//GEN-LAST:event_seiRelogActionPerformed

    private void carAddContentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_carAddContentActionPerformed
          
            //Create a content manager
            ContentManager cm = new ContentManager();
            cm.resetData();//safety procaution
            System.out.println("CON EDIT - Setting ContentManager");
            
            //Set Options
            cm.setOperationType(0);
            cm.setContent(carContentArea.getText());
            cm.setNumberOfSpaces(carNumberOfSpaces.getSelectedIndex());
            cm.setBeforeOrAfter(carBeforeOrAfter.getSelectedIndex());
            cm.setPagesData(carSearchAreaP);
            cm.setSpacesData(carSearchAreaS);
            System.out.println("CON EDIT - SPACE AREA:" + carSearchAreaS.size());
            System.out.println("CON EDIT - Leaving to PerformADD()");
            
            //Perform Add
            cm.setFinalData();
             
    }//GEN-LAST:event_carAddContentActionPerformed

    private void carSpaceSelectorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_carSpaceSelectorItemStateChanged
        //System.out.println("ISC evt: "+evt);
        if(evt.toString().contains("stateChange=SELECTED")){
             try {
                 RemotePageSummary[] data = getActiveService().getPages(getActiveToken(), spaceSummary[carSpaceSelector.getSelectedIndex()].getKey());
                 carPageSelector.removeAllItems();     
             for(int z=0;z<data.length;z++){
                 carPageSelector.addItem(data[z].getTitle());
             }
                car_current_pageSummary=data;
             } catch (java.rmi.RemoteException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
        
    }//GEN-LAST:event_carSpaceSelectorItemStateChanged

    private void spaceSelectorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_spaceSelectorItemStateChanged
    
      if(evt.toString().contains("stateChange=SELECTED")){
             try{
                 RemotePageSummary[] data = getActiveService().getPages(getActiveToken(), spaceSummary[spaceSelector.getSelectedIndex()].getKey());
                 pageSelector.removeAllItems();     
             for(int z=0;z<data.length;z++){
                 pageSelector.addItem(data[z].getTitle());
             }
                 current_pageSummary=data;
             }catch(java.rmi.RemoteException ex){
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
         
    }//GEN-LAST:event_spaceSelectorItemStateChanged

    private void carRemoveContentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_carRemoveContentActionPerformed
                  
            //Create a content manager
            ContentManager cm = new ContentManager();
            cm.resetData();//safety procaution
            int checkMoreThanOne=0;
            
            //Set ContentManager's Options
            cm.setOperationType(1);
            
            if(carR0.isSelected()){
                cm.setRemoveType(0);
                checkMoreThanOne++;
            }
            if(carR1.isSelected()){
                cm.setRemoveType(1);
                checkMoreThanOne++;
            }
            if(carR2.isSelected()){
                cm.setRemoveType(2);
                checkMoreThanOne++;
            }
            if(carR3.isSelected()){
                cm.setRemoveType(3);
                checkMoreThanOne++;
            }
            if(carR4.isSelected()){
                cm.setRemoveType(4);
                checkMoreThanOne++;
            }
            
            if(checkMoreThanOne==1 && (carSearchAreaP.size()>0 || carSearchAreaS.size()>0)){
                cm.setContent(carContentArea.getText());
                cm.setPagesData(carSearchAreaP);
                cm.setSpacesData(carSearchAreaS);
                System.out.println("CON EDIT - SPACE AREA:" + carSearchAreaS.size());
                System.out.println("CON EDIT - Leaving to PerformADD()");

                //Perform Add
                cm.setFinalData();
            }else{
                Display("No Spaces Or Pages Were Specified. Check The \"Spaces & Pages\" Tab Above");
            }
            if(checkMoreThanOne==0){
                Display("You Must Have Atleast One Remove Option Selected. Check The \"Remove Options\" Tab Above");
            }else if(checkMoreThanOne>1){
                Display("You Must Have Only One Remove Option Selected. Check The \"Remove Options\" Tab Above");
            }
            
    }//GEN-LAST:event_carRemoveContentActionPerformed
    public static  void setActiveToken(String x){
        if(x!=null){
            activeToken=x;
        }
    }
    public static void setActiveService(ConfluenceSoapService x){
        if(x!=null){ 
            activeService = x;
        }
    }
    public static  String getActiveToken(){
        String token="";
        token=activeToken;
        return token;
    }
    
    public static ConfluenceSoapService getActiveService(){
        ConfluenceSoapService active;
        active=activeService;
        return active;
    } 
    
    private void Display(String x){
        int mc = JOptionPane.INFORMATION_MESSAGE;
               JOptionPane.showMessageDialog(mainPanel,x, "Confluence Manager", mc);
    }
    public static JPanel getMainPanel(){ 
        return mainPanel;   
    }
    private void refreshGroupSelector() throws RemoteException{
            groupComboBox.removeAll();
            groupComboBox.removeAllItems();
            String[] x;
            x = getActiveService().getGroups(getActiveToken());
            Arrays.sort(x);
            for(int i=0;i<x.length;i++){
                 groupComboBox.addItem(x[i]);
            }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel LoginSystem;
    private javax.swing.JTabbedPane TaskFlow;
    private javax.swing.JButton aboutButton;
    private javax.swing.JTextField acEmailBox;
    private javax.swing.JTextField acFWEmail1Box;
    private javax.swing.JTextField acFWEmail2Box;
    private javax.swing.JTextField acFWEmail3Box;
    private javax.swing.JTextField acFullnameBox;
    private javax.swing.JTextField acIDBox;
    private javax.swing.JTextArea acMessageBox;
    private javax.swing.JCheckBox acNotifyUser;
    private javax.swing.JTextField acPasswordBox;
    private javax.swing.JTextField acSubjectBox;
    private javax.swing.JTextField acUserGroupsBox;
    private javax.swing.JButton adDeleteButton;
    private javax.swing.JTextField adUserBox;
    private javax.swing.JButton addPageButton;
    private javax.swing.JButton addSpaceButton;
    private javax.swing.JCheckBox advOptions;
    private javax.swing.JLabel advOptionsStatus;
    private javax.swing.JButton autgButton;
    private javax.swing.JTextField autgGroupBox;
    private javax.swing.JTextField autgIDBox;
    private javax.swing.JCheckBox automakeCheckBox;
    private javax.swing.JLabel availableStatus;
    private javax.swing.JLabel buildLabel;
    private javax.swing.JButton cangButton;
    private javax.swing.JTextField cangNameBox;
    private javax.swing.JButton carAddAllButton;
    private javax.swing.JButton carAddContent;
    private javax.swing.JButton carAddPageButton;
    private javax.swing.JButton carAddSpaceButton;
    private javax.swing.JComboBox carBeforeOrAfter;
    private javax.swing.JButton carClearButton;
    private javax.swing.JTextArea carContentArea;
    public static javax.swing.JTextField carEditedPages;
    private javax.swing.JComboBox carNumberOfSpaces;
    private javax.swing.JComboBox carPageSelector;
    public static javax.swing.JProgressBar carProgressBar;
    private javax.swing.JCheckBox carR0;
    private javax.swing.JCheckBox carR1;
    private javax.swing.JCheckBox carR2;
    private javax.swing.JCheckBox carR3;
    private javax.swing.JCheckBox carR4;
    private javax.swing.JButton carRemoveContent;
    private javax.swing.JList carSearchAreaList;
    public javax.swing.JComboBox carSpaceSelector;
    public static javax.swing.JTextField carTotalPages;
    private javax.swing.JButton cfuButton;
    private javax.swing.JButton clearButton;
    private javax.swing.JButton connectButton;
    private javax.swing.JButton createButton;
    private javax.swing.JLabel currentStatus;
    private javax.swing.JTextField dagBox;
    private javax.swing.JButton dagButton;
    private javax.swing.JTextField dagReplaceBox;
    private javax.swing.JTextField domainBox;
    private javax.swing.JButton edituserButton;
    private javax.swing.JTextField emailBox;
    private javax.swing.JButton exitButton;
    private javax.swing.JTextField fullnameBox;
    private javax.swing.JComboBox groupComboBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel javaVersionLabel;
    private javax.swing.JTextField loginBox;
    private javax.swing.JButton logoutButton;
    public static javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JTextField nameBox;
    private javax.swing.JCheckBox notificationsCheck;
    private javax.swing.JTextField numberoftimesBox;
    private javax.swing.JComboBox pSpaceSelector;
    private javax.swing.JTextField pVUPBox;
    private javax.swing.JButton pVUPButton;
    private javax.swing.JList pVUPList;
    private javax.swing.JTextField pVUPSpaceBox;
    private javax.swing.JComboBox pageSelector;
    private javax.swing.JPasswordField passwordBox;
    private javax.swing.JTextField pcPasswordBox;
    private javax.swing.JCheckBox perm1;
    private javax.swing.JCheckBox perm10;
    private javax.swing.JCheckBox perm11;
    private javax.swing.JCheckBox perm12;
    private javax.swing.JCheckBox perm13;
    private javax.swing.JCheckBox perm14;
    private javax.swing.JCheckBox perm2;
    private javax.swing.JCheckBox perm3;
    private javax.swing.JCheckBox perm4;
    private javax.swing.JCheckBox perm5;
    private javax.swing.JCheckBox perm6;
    private javax.swing.JCheckBox perm7;
    private javax.swing.JCheckBox perm8;
    private javax.swing.JCheckBox perm9;
    private javax.swing.JButton pmAddAll;
    private javax.swing.JButton pmAddSpace;
    private javax.swing.JTextField pmEntityBox;
    private javax.swing.JTextField pmEntityResultsBox;
    private javax.swing.JTextField pmOperationBox;
    private javax.swing.JComboBox pmOperationSelector;
    private javax.swing.JButton pmPerformButton;
    private javax.swing.JList pmPermList;
    private javax.swing.JProgressBar pmProgressBar;
    private javax.swing.JButton pmRemoveAll;
    private javax.swing.JButton pmRemoveSelected;
    private javax.swing.JTextArea pmReqs;
    private javax.swing.JCheckBox pmSelectAll;
    private javax.swing.JComboBox pmSpaceSelector;
    private javax.swing.JList pmSpaces;
    private javax.swing.JTextField pmSpacesModifiedBox;
    private javax.swing.JTextField pmSpacesSpecifiedBox;
    private javax.swing.JComboBox pmUGSelector;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel psLabel;
    private javax.swing.JLabel psLabel1;
    private javax.swing.JTextField pwIDBox;
    private javax.swing.JTextField reasonBox;
    private javax.swing.JTextField replaceBox;
    private javax.swing.JList resultsList;
    private javax.swing.JButton rufgButton;
    private javax.swing.JTextField rufgGroupBox;
    private javax.swing.JTextField rufgIDBox;
    private javax.swing.JLabel saLabel;
    private javax.swing.JLabel saLabel1;
    private javax.swing.JButton sandrButton;
    private javax.swing.JList searchAreaList;
    private javax.swing.JTextField searchBox;
    private javax.swing.JProgressBar searchProgressBar;
    private javax.swing.JButton searchandrButton;
    private javax.swing.JTextField searchuserBox;
    private javax.swing.JButton searchuserButton;
    private javax.swing.JButton seiDownload;
    private javax.swing.JTextField seiDownloadLink;
    private javax.swing.JProgressBar seiExportBar;
    private javax.swing.JButton seiExportButton;
    private javax.swing.JFileChooser seiFileChooser;
    private javax.swing.JProgressBar seiImportBar;
    private javax.swing.JButton seiImportButton;
    private javax.swing.JButton seiRelog;
    private javax.swing.JComboBox seiSpaceSelector;
    private javax.swing.JTextField senderEmailBox;
    private javax.swing.JPasswordField senderPasswordBox;
    private javax.swing.JTextField senderPortBox;
    private javax.swing.JTextField senderSMTPBox;
    public javax.swing.JComboBox spaceSelector;
    private javax.swing.JLabel ssLabel;
    private javax.swing.JLabel ssLabel1;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JLabel updateStatus;
    private javax.swing.JLabel urlLabel;
    private javax.swing.JTextField urltouserBox;
    private javax.swing.JLabel versionLabel;
    private javax.swing.JLabel versionStatus;
    private javax.swing.JButton vgButton;
    private javax.swing.JTextField vgIDBox;
    private javax.swing.JList vgList;
    // End of variables declaration//GEN-END:variables

    private final Timer messageTimer;
    
    private String myDomain="";
    private final Timer busyIconTimer;
    private final String _Version = "1.9";
    private String _UpdateVersion = "";
    private final Icon idleIcon;
    public int errorCheck=0;
    
    //page/space data
    private RemoteSpaceSummary[] spaceSummary;
    private RemotePageSummary[] current_pageSummary;
    private RemotePageSummary[] car_current_pageSummary;
    private String[] permissionsSummary= new String[14];
    
    private ArrayList<RemotePage> resultsData = new ArrayList<RemotePage>();
    private ArrayList<RemotePage> searchDataFinal = new ArrayList<RemotePage>();
    private ArrayList<RemoteSpaceSummary> searchDataS = new ArrayList<RemoteSpaceSummary>();
    private ArrayList<RemotePageSummary> searchDataP = new ArrayList<RemotePageSummary>();
    private ArrayList<RemoteSpaceSummary> carSearchAreaS = new ArrayList<RemoteSpaceSummary>();
    private ArrayList<RemotePageSummary> carSearchAreaP = new ArrayList<RemotePageSummary>();
    private ArrayList<RemoteSpaceSummary> pmSpaceArea = new ArrayList<RemoteSpaceSummary>();
    
    public int delay10 = 600000;
    public int delay5s = 5000;
    public int delay1s = 1000;
    private boolean consumeCheck=false;
    
    private boolean advOptionsEnabler=false;
    private ConEditEditor frametest;
    public int delay1 = 60000;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private static String activeToken="";
    private static ConfluenceSoapService activeService=null;
    private JDialog aboutBox;
    private boolean autoCreate=false;
    private boolean notifyUser=false;
    private boolean notificationCheck=false;
    private final static String passwordCharacter = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static String EMAIL_SUBJECT = "Welcome!";
    private static String EMAIL_TEMPLATE = 
            "Hello %s,.\n" +
            "Class aptent taciti sociosqu ad litora torquent per conubia nostra.\n" +
            "ed euismod malesuada turpis vel elementum.\n" +
            "Nulla laoreet, felis pretium tincidunt accumsan. \n" +
            "Username: %s \n" +
            "Password: %s\n" +
            "Sincerely,\n" +
            "ConfluenceManager";

    //Password Generator Thanks To Graham M.
    private String generatePassword() {
        StringBuilder toReturn = new StringBuilder(8);
        for (int i = 0; i < 8; i++) {
            toReturn.append(passwordCharacter.charAt((int) Math.floor(Math.random() * passwordCharacter.length())));
        }
        return toReturn.toString();
    }

    public void updateSpaceSummary(){
        try {
            spaceSummary = getActiveService().getSpaces(getActiveToken());
        } catch (RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<RemoteSpaceSummary> arrayToArrayList(RemoteSpaceSummary[] test){
       
       ArrayList<RemoteSpaceSummary> data = new ArrayList<RemoteSpaceSummary>();
       
       for(int i=0;i<test.length;i++){
            data.add(test[i]);
       }
       
       return data;
    }
    
    private void flipLoginSystem(boolean active){
        if(active){
             domainBox.setEnabled(false);
             passwordBox.setEnabled(false);
             loginBox.setEnabled(false);
             logoutButton.setEnabled(true);
             connectButton.setEnabled(false);
             flipTaskFlow();
        }else{
             domainBox.setEnabled(true);
             passwordBox.setEnabled(true);
             loginBox.setEnabled(true);
             logoutButton.setEnabled(false);
             connectButton.setEnabled(true);
             TaskFlow.setSelectedIndex(6);
             flipTaskFlow();
        }
    }
    private void flipTaskFlow(){
        for(int i=0;i<TaskFlow.getTabCount()-1;i++){
            TaskFlow.setEnabledAt(i, !TaskFlow.isEnabledAt(i));
        }  
    }
    private boolean loginToSystem()
    {
        try {
            
            ConfluenceSoapService service = new ConfluenceSoapServiceServiceLocator(myDomain).getConfluenceserviceV1();
            String id = loginBox.getText();
            char[] pass = passwordBox.getPassword();
            StringBuilder test = new StringBuilder();
            String password="";
            for(int i=0;i<pass.length;i++){test.append(pass, i, 1);}
            password=test.toString();
            String token = service.login(id,password);
            setActiveToken(token);
            setActiveService(service);
            
        } catch (RemoteException ex) {
            Display("Login Failed.");
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (ServiceException ex) {
            Display("Login Failed.");
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    private boolean domainCheck(String x){
        boolean test = true;
            String testDomain=x;
            testDomain=testDomain.replace("https://", "");
            testDomain=testDomain.replace("http://", "");
            boolean dTest=false;
            InetAddress inetAddress;
            try {
                inetAddress = InetAddress.getByName(testDomain);
            } catch (UnknownHostException ex) {
                test=false;
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
        return test;
    }
    public String getMyDomain(){
        return myDomain;
    }
    
    public void setMyDomain(String x){
        myDomain = x;
    }
    private boolean userExists(String text){
        try {
            return(getActiveService().hasUser(getActiveToken(), text));
        } catch (java.rmi.RemoteException ex) {
            Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return false;
    }
    private void setUpdateVersion(String x){
        _UpdateVersion =x;
    }
    private String getUpdateVersion(){
       return (_UpdateVersion);
    }
    ActionListener RelogTask = new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
            try{
            if(!connectButton.isEnabled())
            {
                setActiveService(new ConfluenceSoapServiceServiceLocator(myDomain).getConfluenceserviceV1());
                loginToSystem();
                System.out.println("Service & Token Updated. You Are Logged In Still.");
            }else{}
            } catch (ServiceException ex) {
                Logger.getLogger(ConEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
      }
    };
    
     ActionListener ExportSpaceTask = new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
          
          if(seiExportBar.getValue()==100){
              seiExportBar.setValue(0);
          }
          seiExportBar.setValue(seiExportBar.getValue()+1);
      }
    };
     
     ActionListener ImportSpaceTask = new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
          if(seiExportBar.getValue()==100){
              seiExportBar.setValue(0);
          }
            seiImportBar.setValue(seiImportBar.getValue()+1);
      }
    };
     
      ActionListener CloseTask = new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
            System.exit(0);
      }
    };

    private String[] enlargeArray(String[] x) {
        String[] clone = new String[x.length*2];
        for(int i=0;i<x.length;i++){
            clone[i]=x[i];
        }
        return clone;
    }
    

    private boolean checkIfPageHasParent(RemotePageSummary page, ArrayList<RemoteSpaceSummary> rss) throws java.rmi.RemoteException {
        boolean test=false; 
        for(RemoteSpaceSummary x : rss){
            RemotePageSummary[] pagesInCurrent = getActiveService().getPages(getActiveToken(), x.getKey());
            for(int i=0;i<pagesInCurrent.length;i++){
                if(pagesInCurrent[i].getTitle().equalsIgnoreCase(page.getTitle())){
                    test=true;
                    //System.out.println("Pages parent space already in search");
                    advOptionsStatus.setText("The Page You Are Trying To Add Is Already In A Space In The Search Area");   
                }
            }   
        }
        return test;
    }

    private boolean checkIfSpaceHasChildren(RemoteSpaceSummary w, ArrayList<RemotePageSummary> rps) throws java.rmi.RemoteException {
        boolean test=false;
        RemotePageSummary[] childrenOfSpace = getActiveService().getPages(getActiveToken(), w.getKey());
        for(int x=0;x<rps.size();x++){    
            for(int z=0;z<childrenOfSpace.length;z++){
                if(!rps.isEmpty() && childrenOfSpace.length!=0){
                    if(rps.get(x).getTitle().equalsIgnoreCase(childrenOfSpace[z].getTitle())){
                        advOptionsStatus.setText("Space Added & Pages Under The Space Were Removed");
                        rps.remove(x);
                        test=true; 
                    }
                }
            }   
        }
        return test;
    }
    
    private String[] setSearchAreaDisplaySF(){
        String[] value=new String[searchDataP.size()+searchDataS.size()];
        ArrayList<String> temp = new ArrayList<String>();
        searchAreaList.setListData(value);
        //System.out.println("___");
        for(RemoteSpaceSummary y : searchDataS){
            temp.add(y.getName());   
            //System.out.println(y.getName());
        }
        for(RemotePageSummary x : searchDataP){
            temp.add(x.getTitle());
            //System.out.println(x.getTitle());
        }
        for(int x=0;x<temp.size();x++){
            value[x]=temp.get(x);
            //System.out.println("t:"+value[x]);
        }
        searchAreaList.removeAll();
        searchAreaList.setListData(value);
        return value;
    }
    
    private String[] setSearchAreaDisplayCAR(){
        String[] value=new String[carSearchAreaP.size()+carSearchAreaS.size()];
        ArrayList<String> temp = new ArrayList<String>();
        carSearchAreaList.setListData(value);
        //System.out.println("___");
        for(RemoteSpaceSummary y : carSearchAreaS){
            temp.add(y.getName());   
            //System.out.println(y.getName());
        }
        for(RemotePageSummary x : carSearchAreaP){
            temp.add(x.getTitle());
            //System.out.println(x.getTitle());
        }
        for(int x=0;x<temp.size();x++){
            value[x]=temp.get(x);
            //System.out.println("t:"+value[x]);
        }
        carSearchAreaList.removeAll();
        carSearchAreaList.setListData(value);
        return value;
    }
    
    private String[] setResultsListDisplaySF(){
        String[] data=new String[resultsData.size()];
           for(int w=0;w<resultsData.size();w++){
               data[w]=resultsData.get(w).getTitle();
           }
        
        return data;
    }

    private void setpmSpaces() {

        String[] x = new String[pmSpaceArea.size()];
        for(int i=0;i<pmSpaceArea.size();i++){
            x[i]=pmSpaceArea.get(i).getName();
        }
        pmSpaces.setListData(x);
    }
    private String[] pmBuildPermissionVectorArray() {
        int counter=0;
        String[] data;
        if(perm1.isSelected()){
            counter++;
        }        if(perm2.isSelected()){
            counter++;
        }        if(perm3.isSelected()){
            counter++;
        }        if(perm4.isSelected()){
            counter++;
        }        if(perm5.isSelected()){
            counter++;
        }        if(perm6.isSelected()){
            counter++;
        }        if(perm7.isSelected()){
            counter++;
        }        if(perm8.isSelected()){
            counter++;
        }        if(perm9.isSelected()){
            counter++;
        }        if(perm10.isSelected()){
            counter++;
        }        if(perm11.isSelected()){
            counter++;
        }        if(perm12.isSelected()){
            counter++;
        }        if(perm13.isSelected()){
            counter++;
        }        if(perm14.isSelected()){
            counter++;
        }
        data = new String[counter];
        return data;
    }
    private String[] pmBuildPermissionVector(){
        String[] data = pmBuildPermissionVectorArray();
        if(data.length==0){
            return permissionsSummary;
        }
        int index=0;
        if(perm1.isSelected()){
            data[index]=permissionsSummary[0];
            index++;
        }        if(perm2.isSelected()){
            data[index]=permissionsSummary[1];
            index++;
        }        if(perm3.isSelected()){
            data[index]=permissionsSummary[2];
            index++;
        }        if(perm4.isSelected()){
            data[index]=permissionsSummary[3];
            index++;
        }        if(perm5.isSelected()){
            data[index]=permissionsSummary[4];
            index++;
        }        if(perm6.isSelected()){
            data[index]=permissionsSummary[5];
            index++;
        }        if(perm7.isSelected()){
            data[index]=permissionsSummary[6];
            index++;
        }        if(perm8.isSelected()){
            data[index]=permissionsSummary[7];
            index++;
        }        if(perm9.isSelected()){
            data[index]=permissionsSummary[8];
            index++;
        }        if(perm10.isSelected()){
            data[index]=permissionsSummary[9];
            index++;
        }        if(perm11.isSelected()){
            data[index]=permissionsSummary[10];
            index++;
        }        if(perm12.isSelected()){
            data[index]=permissionsSummary[11];
            index++;
        }        if(perm13.isSelected()){
            data[index]=permissionsSummary[12];
            index++;
        }        if(perm14.isSelected()){
            data[index]=permissionsSummary[13];
            index++;
        }
        return data;
    }
    private boolean pmEntityCheck() throws RemoteException {
       if(pmUGSelector.getSelectedIndex()==0){
           if(getActiveService().hasUser(getActiveToken(), pmEntityBox.getText())){
               return true;
           }else{
               Display("User Not Found.");
               return false;
           }
       }
       if(pmUGSelector.getSelectedIndex()==1){  
           if(getActiveService().hasGroup(getActiveToken(), pmEntityBox.getText())){
               return true;
           }else{
               Display("Group Not Found.");
               return false;
           }
       }
       return false;
    }
    private boolean pmPermCheck() {
        int counter=0;
        if(perm1.isSelected()){
            counter++;
        }        if(perm2.isSelected()){
            counter++;
        }        if(perm3.isSelected()){
            counter++;
        }        if(perm4.isSelected()){
            counter++;
        }        if(perm5.isSelected()){
            counter++;
        }        if(perm6.isSelected()){
            counter++;
        }        if(perm7.isSelected()){
            counter++;
        }        if(perm8.isSelected()){
            counter++;
        }        if(perm9.isSelected()){
            counter++;
        }        if(perm10.isSelected()){
            counter++;
        }        if(perm11.isSelected()){
            counter++;
        }        if(perm12.isSelected()){
            counter++;
        }        if(perm13.isSelected()){
            counter++;
        }        if(perm14.isSelected()){
            counter++;
        }
        if(counter>=1){
            return true;
        }else{
            Display("No Permissions Were Selected.");
            return false;
        }
    }
    private boolean pmSpaceCheck() {
        if(pmSpaceArea.size()>=1){
            return true;
        }else{
            Display("No Spaces Were Found.");
            return false;
        }
    }


}
